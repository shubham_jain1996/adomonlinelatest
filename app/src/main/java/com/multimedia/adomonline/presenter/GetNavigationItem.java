package com.multimedia.adomonline.presenter;


import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;
import com.multimedia.adomonline.model.modelClass.player.SideMenuList;

import java.util.List;

public interface GetNavigationItem {
    interface OnFinishedListener {
        void sideMenuListItem(List<SideMenuList> sideMenuData);
    }

    void getNavigationItem(OnFinishedListener listener);

}
