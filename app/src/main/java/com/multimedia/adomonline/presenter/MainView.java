package com.multimedia.adomonline.presenter;

import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;
import com.multimedia.adomonline.model.modelClass.player.SideMenuList;

import java.util.List;

public interface MainView {

    void showProgress();

    void hideProgress();

    void setSideMenuList(List<SideMenuList> sideMenuList);
}
