package com.multimedia.adomonline.presenter;

import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.modelClass.GetNonce;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;
import com.multimedia.adomonline.model.modelClass.player.SideMenuList;
import com.multimedia.adomonline.model.modelClass.player.SubMenuList;
import com.multimedia.adomonline.model.network.DataService;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GetNavigationItemImpl implements GetNavigationItem{
    List<HomeClass> BannerData= new ArrayList<>();
    List<SideMenuList> sideListData= new ArrayList<>();
    List<SubMenuList> submenu= new ArrayList<>();

    public List<SubMenuList> addSubMenuData(){
      //  submenu.add(new SubMenuList("Live Radio",0));
       // submenu.add(new SubMenuList("Live TV",1));
        submenu.add(new SubMenuList("Videos",2));


        return submenu;
    }

    @Override
    public void getNavigationItem(OnFinishedListener listener) {
        listener.sideMenuListItem(getSideListData());

    }



    private  List<SideMenuList> getSideListData(){
        sideListData.add(new SideMenuList("Home",R.drawable.ic_home,0,false,false,null));
        sideListData.add(new SideMenuList("News",R.drawable.ic_news,32,false,false,null));
        sideListData.add(new SideMenuList("Politics",R.drawable.ic_politice,40,false,false,null));
        sideListData.add(new SideMenuList("Business",R.drawable.ic_business,18,false,false,null));
        sideListData.add(new SideMenuList("Entertainment",R.drawable.ic_entertainment,19,false,false,null));
        sideListData.add(new SideMenuList("Sports",R.drawable.ic_sports,46,false,false,null));
        sideListData.add(new SideMenuList("Opinion",R.drawable.ic_opinion,37,false,false,null));
        sideListData.add(new SideMenuList("Photo Galleries",R.drawable.ic_photo,7858,false,false,null));
      //  sideListData.add(new SideMenuList("Breaking News",R.drawable.ic_breaking_news,7937,false,false,null));
      //  sideListData.add(new SideMenuList("Editorial",R.drawable.ic_editorial,7939,false,false,null));
        sideListData.add(new SideMenuList("Newspaper Headlines",R.drawable.ic_headlines,7964,false,false,null));
        sideListData.add(new SideMenuList("Elections",R.drawable.ic_election,60,false,false,null));
        sideListData.add(new SideMenuList("Technology",R.drawable.ic_technology,49,false,false,null));
        sideListData.add(new SideMenuList("Lifestyle",R.drawable.ic_lifestyle,28,false,false,null));
        sideListData.add(new SideMenuList("Automobile",R.drawable.ic_automobile,17,false,false,null));
        sideListData.add(new SideMenuList("Infographic",R.drawable.ic_infographic,26,false,false,null));
     //   sideListData.add(new SideMenuList("Real-Estate",R.drawable.ic_realestate,43,false,false,null));
      //  sideListData.add(new SideMenuList("Services",R.drawable.ic_services,45,false,false,null));
        sideListData.add(new SideMenuList("World",R.drawable.ic_world,52,false,false,null));
        sideListData.add(new SideMenuList("Media",R.drawable.ic_media,1,false,true,addSubMenuData()));

        return sideListData;
    }

}
