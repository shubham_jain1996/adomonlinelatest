package com.multimedia.adomonline.view.search;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.adapter.searchAdapter.SearchItemAdapter;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;
import com.multimedia.adomonline.model.network.DataService;
import com.multimedia.adomonline.model.utility.RecyclerTouchListener;
import com.multimedia.adomonline.view.mainActivity.Article;
import com.multimedia.adomonline.view.mainActivity.fragment.Home;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends AppCompatActivity {
    @BindView(R.id.locationList)
    RecyclerView locationList;

    @BindView(R.id.location)
    EditText location;

    @BindView(R.id.cancel)
    TextView cancel;

    @BindView(R.id.progress_view)
    CircularProgressView progress_view;

    @BindView(R.id.noLocationFound)
    TextView nolocationFound;
    private LinearLayoutManager layoutManager;
    private SearchItemAdapter mAdapter;
    Handler mHandler;
    List<HomeClass> searchItem= new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        progress_view.setVisibility(View.INVISIBLE);
        mHandler = new Handler(Looper.getMainLooper());
        setUpRecyclerView();
        initiatetextListener();
    }
    long delay = 1000;
    long last_text_edit = 0;
    Handler handler = new Handler();

    private Runnable input_finish_checker = new Runnable() {
        public void run() {
            if (System.currentTimeMillis() > (last_text_edit + delay - 500)) {
                if (location.getText().toString().trim().length() == 0) {
                    searchItem.clear();
                    mAdapter.notifyDataSetChanged();
                } else {
                    progress_view.setVisibility(View.VISIBLE);
                    getLocationDataFromServer(location.getText().toString().trim());
                }

            }
        }
    };
    private void initiatetextListener() {
        location.addTextChangedListener(new TextWatcher() {

            boolean isTyping = false;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                handler.removeCallbacks(input_finish_checker);
            }



            @Override
            public void afterTextChanged( Editable s) {
                if (location.getText().toString().trim().length() > 0) {
                    last_text_edit = System.currentTimeMillis();
                    handler.postDelayed(input_finish_checker, delay);
                } else {
                    searchItem.clear();
                    mAdapter.notifyDataSetChanged();
                }

            }

        });
    }
    private void getLocationDataFromServer(String locationname) {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<List<HomeClass>>() {
            @Override
            public void onDataSuccess(List<HomeClass> response ,int code) {
                progress_view.setVisibility(View.GONE);
                if (response.size()>0){
                    locationList.setVisibility(View.VISIBLE);
                    nolocationFound.setVisibility(View.GONE);
                    searchItem.clear();
                    searchItem.addAll(response);
                    mAdapter.notifyDataSetChanged();
                    locationList.scrollToPosition(0);
                }else {
                    locationList.setVisibility(View.GONE);
                    nolocationFound.setVisibility(View.VISIBLE);
                    searchItem.clear();
                    mAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onDataFailiure() {
                progress_view.setVisibility(View.GONE);
            }
        });
        dataService.getSearchResult(locationname);
    }
    private void setUpRecyclerView() {
        locationList.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        locationList.setLayoutManager(layoutManager);
        mAdapter= new SearchItemAdapter(this,searchItem);
        locationList.setAdapter(mAdapter);
        locationList.addOnItemTouchListener(new RecyclerTouchListener(this, locationList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position1) {
                Intent i = new Intent(SearchActivity.this, Article.class);
             /*   i.putExtra("categoryType","news");
                i.putExtra("model",  searchItem.get(position1));
                i.putExtra("position",  position1);
                i.putParcelableArrayListExtra("data", (ArrayList<? extends Parcelable>) searchItem);*/
                i.putExtra("categoryType","News");
                i.putExtra("model", searchItem.get(position1));
                i.putExtra("position",  position1);
                i.putParcelableArrayListExtra("data", (ArrayList<? extends Parcelable>) searchItem);
                startActivity(i);
              //  setResult(RESULT_OK, i);
                finish();



            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @OnClick(R.id.cancel)
    public void finishActivity(){
        finish();
    }
}
