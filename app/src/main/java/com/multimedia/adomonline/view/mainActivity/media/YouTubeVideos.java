package com.multimedia.adomonline.view.mainActivity.media;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.adapter.youtube.YouTubeVedioAdapter;
import com.multimedia.adomonline.model.modelClass.youtubeFiles.Item;
import com.multimedia.adomonline.model.modelClass.youtubeFiles.YoutubeVedioList;
import com.multimedia.adomonline.model.network.DataService;
import com.multimedia.adomonline.model.utility.RecyclerTouchListener;
import com.multimedia.adomonline.view.mainActivity.MainActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class YouTubeVideos extends AppCompatActivity implements YouTubePlayer.OnInitializedListener, YouTubeVedioAdapter.callMore {
    String playlistId;
    @BindView(R.id.vedioList)
    RecyclerView playListsView;
    private static final int RECOVERY_REQUEST = 1;
    private LinearLayoutManager layoutManager;
    private YouTubeVedioAdapter mAdapter;
    String token;
    YouTubePlayer youTubePlayer;
    boolean wasRestored;
    int playVedioPosition;
    String type;
    YouTubeVedioAdapter.callMore callMore;
    private boolean isLoading = false;
    List<Item> youTubeVedios = new ArrayList<>();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private AdView mAdView;
    private InterstitialAd adView;
    boolean isNextAvailable = true;
    boolean isFullScreen = false;
    boolean isFirstTime = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_tube_videos);
        ButterKnife.bind(this);
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        setUpRecyclerView();
        setUpToolbar();
        this.callMore = this;
        type = getIntent().getStringExtra("type");
        if (type.equals("more")) {
            playlistId = getIntent().getStringExtra("playlistId");
            playVedioPosition = 0;
        } else {
            playlistId = getIntent().getStringExtra("playlistId");
            playVedioPosition = getIntent().getIntExtra("position", 0);

        }
        YouTubePlayerSupportFragment frag = (YouTubePlayerSupportFragment) getSupportFragmentManager().findFragmentById(R.id.youtube_fragment);
        frag.initialize(getResources().getString(R.string.YouTubekey), this);
        adView = new InterstitialAd(this);
        adView.setAdUnitId(getResources().getString(R.string.interId));
        setUpInterStialAd();
        getYouTubeVedios();

    }

    private void setUpInterStialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();

            }

            @Override
            public void onAdFailedToLoad(int i) {
                Log.d("satyam", "onAdFailedToLoad: " + i);
            }
        });
    }

    private void setUpToolbar() {
        ImageView back = toolbar.findViewById(R.id.backPressed);
        TextView title = toolbar.findViewById(R.id.title);
        title.setText("Videos");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getYouTubeVedios() {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<YoutubeVedioList>() {
            @Override
            public void onDataSuccess(YoutubeVedioList response, int code) {
                isLoading = false;
                if (response != null) {
                    int size = youTubeVedios.size();
                    youTubeVedios.addAll(response.getItems());
                    mAdapter.notifyItemInserted(size);
                    if (response.getNextPageToken() != null) {
                        token = response.getNextPageToken();
                        isNextAvailable = true;
                    } else {
                        isNextAvailable = false;
                    }
                    if (isFirstTime) {
                        playYouTubeVedio();
                        isFirstTime = false;
                    }

                }

            }

            @Override
            public void onDataFailiure() {
                isLoading = false;
            }
        });
        dataService.getYouTubeVedios(playlistId, token);
    }

    private void playYouTubeVedio() {
        if ( youTubePlayer !=null && !wasRestored) {
            youTubePlayer.loadVideo(youTubeVedios.get(playVedioPosition).getSnippet().getResourceId().getVideoId());
            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
        }
        type = "more";


    }

    private void setUpRecyclerView() {
        playListsView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        RecyclerView.LayoutManager lm = new GridLayoutManager(this, 2);
        playListsView.setLayoutManager(lm);
        mAdapter = new YouTubeVedioAdapter(this, youTubeVedios, this);
        playListsView.setAdapter(mAdapter);
        playListsView.addOnItemTouchListener(new RecyclerTouchListener(this, playListsView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position1) {
                if (MainActivity.adCounter % 5 == 0) {
                    adView.show();
                }
                MainActivity.adCounter++;
                if (!wasRestored) {
                    youTubePlayer.loadVideo(youTubeVedios.get(position1).getSnippet().getResourceId().getVideoId());
                    youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        playListsView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();

                boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;

                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isLoading) {
                        if (isNextAvailable) {
                            getYouTubeVedios();
                        } else {
                            Toast.makeText(YouTubeVideos.this, "No more videos", Toast.LENGTH_SHORT).show();
                        }
                    }
                    isLoading = true;

                }
            }
        });
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        this.youTubePlayer = youTubePlayer;
        this.wasRestored = wasRestored;
        youTubePlayer.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {
            @Override
            public void onFullscreen(boolean b) {
                isFullScreen = b;
            }
        });

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = String.format("Error initializing YouTube player", errorReason.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void callMoreData() {

    }

    @Override
    public void onBackPressed() {
        if (isFullScreen) {
            youTubePlayer.setFullscreen(false);
        } else {
            super.onBackPressed();
        }

    }
}