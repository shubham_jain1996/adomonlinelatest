package com.multimedia.adomonline.view.mainActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;
import com.multimedia.adomonline.model.modelClass.bannerData.Bannerdatamodel;
import com.multimedia.adomonline.model.network.DataService;
import com.multimedia.adomonline.model.util.Util;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Article extends AppCompatActivity {


    private static final int VUUKEL_RESULT =1 ;
    HomeClass articleData;
    List<Bannerdatamodel> articleImage = new ArrayList<>();

    @BindView(R.id.webview)
    WebView webView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private AdView mAdView;


    @BindView(R.id.progress_view)
    CircularProgressView prograssBar;

    String categoryType;
    String mediaUrl = "";

    HomeClass homeData;
    private InterstitialAd adView;

    List<HomeClass> relatedArticle= new ArrayList<>();
    int itemposition;
    private String text;
    private int loop;
    private String ImagePath;

    CallbackManager callbackManager;
    ShareDialog shareDialog;
    boolean isSlug=false;
    private String id;


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        ButterKnife.bind(this);
        relatedArticle=getIntent().getExtras().getParcelableArrayList("data");
        homeData=getIntent().getParcelableExtra("model");
        categoryType=getIntent().getExtras().getString("categoryType");
        itemposition=getIntent().getExtras().getInt("position");
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        setUpToolBar();
        if (homeData.getSource_url()!=null){
            mediaUrl=homeData.getSource_url();

        }else{
            getArticleImages(homeData.getFeaturedMedia());
        }
        getArticleDetails(homeData.getId());

        setUpWebView();
        if (MainActivity.adCounter %5 == 0) {
            setUpInterStialAd();
        }

    }
    private void setUpInterStialAd() {
        adView = new InterstitialAd(this);
        adView.setAdUnitId(getResources().getString(R.string.interId));
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.show();
            }
            @Override
            public void onAdFailedToLoad(int i) {

            }
        });
    }

    private void setUpToolBar() {
        ImageView backPressed= toolbar.findViewById(R.id.backPressed);
        backPressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @SuppressLint("SetJavaScriptEnabled")
    private void setUpWebView() {
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.getSettings().setDomStorageEnabled(true);



        if (Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
        } else {
            CookieManager.getInstance().setAcceptCookie(true);
        }


        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                for (int i =0; i< relatedArticle.size(); i++){
                    if (relatedArticle.get(i).getSlug().equals(url)){
                        itemposition=i;
                    }
                }
                Uri uri = Uri.parse(url);
                String path = uri.getPath();
                String limit = uri.getQueryParameter("post");
                if (limit!=null){
                    getArticleDetailsUsingId(limit);
                }else {
                    getArticleDetailsUsingSlug(path);
                }


                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
               prograssBar.setVisibility(View.VISIBLE);
            }

            public void onPageFinished(WebView view, String url) {
                prograssBar.setVisibility(View.GONE);
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                prograssBar.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {

            }
        });

    }

   private void getArticleDetails(Integer id) {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<HomeClass>() {
            @Override
            public void onDataSuccess(HomeClass response ,int code) {
                if (response != null) {
                    articleData = response;
                    setupContent();
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.getArticleDetails(String.valueOf(id));
    }

    private void getArticleDetailsUsingId(String id) {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<HomeClass>() {
            @Override
            public void onDataSuccess(HomeClass response ,int code) {
                if (response != null) {
                    articleData = response;
                    getArticleImages(articleData.getFeaturedMedia());
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.getArticleDetails(id);
    }

    private void  getArticleDetailsUsingSlug(String slug) {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<List<HomeClass>>() {
            @Override
            public void onDataSuccess(List<HomeClass> response ,int code) {
                if (response != null && response.size()>0) {
                    articleData = response.get(0);
                    getArticleImages(articleData.getFeaturedMedia());
                  // setupContent();
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.getArticleDetailsUsingSlug(slug);
    }


    private void getArticleImages(Integer featuredMedia) {
        DataService dataService = new DataService();
        dataService.setOnServerListener(new DataService.ServerResponseListner<Bannerdatamodel>() {
            @Override
            public void onDataSuccess(Bannerdatamodel response,int code) {
                if (response != null) {
                    articleImage.add(response);
                        mediaUrl = response.getSourceUrl();
                        setupContent();
                }else{
                    setupContent();}
            }

            @Override
            public void onDataFailiure() {
            }
        });
        dataService.getBannerData(String.valueOf(featuredMedia));
    }

    private void setupContent()  {
        try {
            InputStream is = getAssets().open("content.html");
            int size = is.available();

            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            String str = new String(buffer);
            StringBuilder stringBuilder = new StringBuilder();
            for (int i=0; i<relatedArticle.size(); i++)
            {

                if (relatedArticle.get(i).getMedia_url()!=null){
                     ImagePath=relatedArticle.get(i).getMedia_url();
                }else{
                    ImagePath="file:///android_asset/new_adom_logo.png";
                }
                Log.d("Shubham", "setupContent: "+relatedArticle.get(i).getMedia_url());
                if (i!=itemposition) {
                    stringBuilder.append("<a href=" + relatedArticle.get(i).getSlug() + "><div class=\"related\">" +
                            "<span><img class=\"relatedar\" src=" + ImagePath + "></span>" +
                            "<span class=\"text\">" + relatedArticle.get(i).getTitle().getRendered() + "</span>" +
                            "</div></a>");
                }
            }
            text = String.format(str, mediaUrl, categoryType, Util.parseDatefromServer(articleData.getDate()), articleData.getTitle().getRendered(), articleData.getContent().getRendered(),stringBuilder.toString());
            webView.loadDataWithBaseURL("file:///android_asset/", text, "text/html", "UTF-8", null);

        }
        catch (Exception e){

        }
    }

    @OnClick(R.id.backPressed)
    public void backButton(){
        finish();

    }

    @OnClick(R.id.share)
    public void shareNews(){
        shareIntent();
    }

    @OnClick(R.id.whatsapp)
    public void shareNewsOnWhatsapp(){
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, articleData.getGuid().getRendered());
        try {
            startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "Whatsapp have not been installed.", Toast.LENGTH_SHORT).show();

        }
    }

    @OnClick(R.id.twitter)
    public void shareNewsOnTwitter(){
        shareTwitter(articleData.getGuid().getRendered());
    }
    private void shareTwitter(String message) {
        Intent tweetIntent = new Intent(Intent.ACTION_SEND);
        tweetIntent.putExtra(Intent.EXTRA_TEXT, message);
        tweetIntent.setType("text/plain");

        PackageManager packManager = getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;
        for (ResolveInfo resolveInfo : resolvedInfoList) {
            if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                tweetIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name);
                resolved = true;
                break;
            }
        }
        if (resolved) {
            startActivity(tweetIntent);
        } else {
            Intent i = new Intent();
            i.putExtra(Intent.EXTRA_TEXT, message);
            i.setAction(Intent.ACTION_VIEW);
            i.setData(Uri.parse("https://twitter.com/intent/tweet?text=" + urlEncode(message)));
            startActivity(i);
            Toast.makeText(this, "Twitter app isn’t found", Toast.LENGTH_LONG).show();
        }
    }

    private String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {

            return "";
        }
    }
    private void shareIntent() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Adom Online");
            String shareMessage= "\n\n";
            shareMessage = shareMessage + articleData.getGuid().getRendered();
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch(Exception e) {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adView != null) {
            adView.setAdListener(null);
        }
    }

    @OnClick(R.id.vuukelComment)
    public void openVuukelFragment(){
        Intent i = new Intent(Article.this, Vuukel.class);
        i.putExtra("articleId",String.valueOf(articleData.getId()));
        startActivityForResult(i,VUUKEL_RESULT);

    }

    @OnClick(R.id.facebookShare)
    public void sharefacebook(){
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(homeData.getSlug()))
                .setQuote(homeData.getTitle().getRendered())
                .build();
        shareDialog.show(content);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                isSlug=data.getExtras().getBoolean("isSlug");
                id=data.getExtras().getString("id");
                if (isSlug){
                    getArticleDetailsUsingSlug(id);
                }else{
                    getArticleDetailsUsingId(id);

                }


            }


        }
    }
}
