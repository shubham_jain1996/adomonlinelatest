package com.multimedia.adomonline.view.mainActivity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.os.Parcelable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.adapter.CommonAdapter;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;
import com.multimedia.adomonline.model.modelClass.bannerData.Bannerdatamodel;
import com.multimedia.adomonline.model.network.DataService;
import com.multimedia.adomonline.model.utility.ConvertHtmlToString;
import com.multimedia.adomonline.model.utility.DataLoadingInterface;
import com.multimedia.adomonline.model.utility.RecyclerTouchListener;
import com.multimedia.adomonline.view.dialoge.ProgressDialogue;
import com.multimedia.adomonline.view.mainActivity.fragment.OtherTabFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class Details extends AppCompatActivity implements DataLoadingInterface, CommonAdapter.ClickMoreButton {



    @BindView(R.id.recyclerView)
    RecyclerView dataView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;



    @BindView(R.id.progress_view)
    CircularProgressView prograssBar;

    String id;
    private int pageNo=1;
    private List<HomeClass> dataList=new ArrayList<>();
    private List<Bannerdatamodel> dataImages=new ArrayList<>();
    private CommonAdapter sportsNewAdapter;
    private String categoryName;
    ConvertHtmlToString convertHtmlToString;
    private AdView mAdView;
    private InterstitialAd adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        if (getIntent()!=null){
        id=getIntent().getExtras().getString("position");
        categoryName=getIntent().getExtras().getString("categoryType");
        }
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        setUpToolBar();
        setUpRecyclerView();

        prograssBar.setVisibility(View.VISIBLE);

        getBussinessData(id);
        if (MainActivity.adCounter %5 == 0) {
            setUpInterStialAd();
        }
    }

    private void setUpToolBar() {
        ImageView backButton= toolbar.findViewById(R.id.backPressed);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setUpRecyclerView() {
        dataView.setVisibility(View.INVISIBLE);
        dataView.setHasFixedSize(true);
        RecyclerView.LayoutManager verticalLayoutManager = new GridLayoutManager(this, 1);
        dataView.setLayoutManager(verticalLayoutManager);
        sportsNewAdapter = new CommonAdapter(Details.this, dataList,categoryName,this);
        dataView.setAdapter(sportsNewAdapter);

    }

    @Override
    public void onBackPressed() {
        if (convertHtmlToString != null &&  (convertHtmlToString.getStatus() == AsyncTask.Status.RUNNING)) {
                convertHtmlToString.cancel(true);
        }
        super.onBackPressed();

    }

    public void getBussinessData(String Id) {
        DataService dataService = new DataService();
        dataService.setOnServerListener(new DataService.ServerResponseListner<List<HomeClass>>() {
            @Override
            public void onDataSuccess(List<HomeClass> response ,int code) {

                if (response != null) {
                    new ConvertHtmlToString(Details.this).execute(response);

                }
            }

            @Override
            public void onDataFailiure() {
            }
        });
        dataService.getallData(Id, String.valueOf(pageNo));
    }



    @Override
    public void preExecute() {
        prograssBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void postExecute(List<HomeClass> homeClasses) {
        prograssBar.setVisibility(View.GONE);
        dataList.addAll(homeClasses);
        sportsNewAdapter.notifyDataSetChanged();
        dataView.setVisibility(View.VISIBLE);
    }

    @Override
    public void clickMore() {
        pageNo++;
        prograssBar.setVisibility(View.VISIBLE);
        getBussinessData(id);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adView != null) {
            adView.setAdListener(null);
        }
    }
    private void setUpInterStialAd() {
        adView = new InterstitialAd(this);
        adView.setAdUnitId(getResources().getString(R.string.interId));
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.show();
            }
            @Override
            public void onAdFailedToLoad(int i) {

            }
        });
    }
}
