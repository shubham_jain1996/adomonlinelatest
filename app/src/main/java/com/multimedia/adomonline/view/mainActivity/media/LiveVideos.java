package com.multimedia.adomonline.view.mainActivity.media;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.adapter.LiveVideoAdapter;
import com.multimedia.adomonline.model.modelClass.youtubeFiles.HomeRecyclerModel;
import com.multimedia.adomonline.model.modelClass.youtubeFiles.PlayListModel;
import com.multimedia.adomonline.model.modelClass.youtubeFiles.YoutubeVedioList;
import com.multimedia.adomonline.model.network.DataService;
import com.multimedia.adomonline.view.dialoge.ProgressDialogue;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LiveVideos extends AppCompatActivity {
    @BindView(R.id.recyclerView)
    RecyclerView videos;

    private List<PlayListModel> playListModelList=new ArrayList<>();
    List<HomeRecyclerModel> homeData = new ArrayList<>();
    ProgressDialogue helpDialog;
    private LiveVideoAdapter mainAdapter;

    @BindView(R.id.toolBar)
    Toolbar toolbar;
    private AdView mAdView;
    private InterstitialAd adView;

    YoutubeVedioList youtubeVedioList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_videos);
        ButterKnife.bind(this);
        helpDialog= new ProgressDialogue(this);
        helpDialog.setCancelable(false);
        helpDialog.show();
        setUpToolBar();
        loadAd();
        setUpRecyclerView();
        setUpModelClassData();
        for (int i = 0; i < playListModelList.size(); i++) {
            PlayListModel data = playListModelList.get(i);
            //  new GetHomebackground(this).execute(data);
            getYouTubeVedios(playListModelList.get(i));
        }
        adView = new InterstitialAd(this);
        adView.setAdUnitId(getResources().getString(R.string.interId));
        setUpInterStialAd();


    }



    private void loadAd() {
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void setUpToolBar() {
        TextView title= toolbar.findViewById(R.id.title);
        title.setText("Videos");
        ImageView backPressesd= toolbar.findViewById(R.id.backPressed);
        backPressesd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    private void setUpInterStialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
              /* if (adCounter%5==0) {
                   adView.show();
               }*/
            }
            @Override
            public void onAdFailedToLoad(int i) {
                Log.d("satyam", "onAdFailedToLoad: "+i);
            }
        });
    }
    public void showAdd()
    {
        if (adView.isLoaded()){
            adView.show();
        }
    }

    private void getYouTubeVedios(PlayListModel playListModel) {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<YoutubeVedioList>() {
            @Override
            public void onDataSuccess(YoutubeVedioList response, int code) {
                if (response!=null){
                    //youtubeVedioList=response;
                 //   playListModelList.add(new PlayListModel(youtubeVedioList));
                  //  homeData.add(new HomeRecyclerModel(title,playListId,response));
                    playListModel.setTopStories(response);
                    mainAdapter.notifyDataSetChanged();

                }
                if (helpDialog.isShowing()){
                    helpDialog.dismiss();
                }

            }

            @Override
            public void onDataFailiure() {
                if (helpDialog.isShowing()){
                    helpDialog.dismiss();
                }
            }
        });
        dataService.getYouTubeVedios4(playListModel.getPlayListId());
    }


    private void setUpRecyclerView() {
        videos.setHasFixedSize(true);
        RecyclerView.LayoutManager lm = new GridLayoutManager(this, 1);
        videos.setLayoutManager(lm);
        mainAdapter = new LiveVideoAdapter(this, playListModelList);
        videos.setAdapter(mainAdapter);


    }
    private void setUpModelClassData() {
        List<PlayListModel> playListModel = new ArrayList<>();
        playListModel.add(new PlayListModel("Badwam"	,"PLyie0rQsQVfKS_g0W8Yxc8sc4TOezqZav","https://i.ytimg.com/vi/7G0ztHUvSu8/default.jpg",youtubeVedioList));
        playListModel.add(new PlayListModel("Badwam Newspaper Review"	,"PL432FA08578B9EFFF","https://i.ytimg.com/vi/RDDZpaJ4fsI/sddefault.jpg",youtubeVedioList));
        playListModel.add(new PlayListModel("Badwam Asem Kese"	,"PLyie0rQsQVfJgP-Xq86MMqu2UJVuLe70L","https://i.ytimg.com/vi/cpGW5Ua_r2w/default.jpg",youtubeVedioList));
        playListModel.add(new PlayListModel("Badwam Sports"	,"PLAB2A86E8C378B928","https://i.ytimg.com/vi/5v_3HNiiyoI/default.jpg",youtubeVedioList));
        playListModel.add(new PlayListModel("Afisem"	,"PLyie0rQsQVfKQswydQt0svPVzONWGfWFT","https://i.ytimg.com/vi/UqbIDr0JCoQ/default.jpg",youtubeVedioList));
        playListModel.add(new PlayListModel("Fire 4 Fire"	,"PL2145582F828FC6E0","https://i.ytimg.com/vi/oUORPMlZh24/default.jpg",youtubeVedioList));
        playListModel.add(new PlayListModel("Adom Kasee"	,"PLyie0rQsQVfI3M4eUgwLtHWoxAtL2aAp2","https://i.ytimg.com/vi/BCv2Xe7M-18/sddefault.jpg",youtubeVedioList));
        playListModel.add(new PlayListModel("Premotobre Kasee"	,"PLyie0rQsQVfL_cTrHt-INe_wv2_U5B_V3","https://i.ytimg.com/vi/1lOAKoKSlTA/default.jpg",youtubeVedioList));
        playListModel.add(new PlayListModel("Nkwa Hia"	,"PLyie0rQsQVfI4e83s2JIHBJ4NkGAEyK6F","https://i.ytimg.com/vi/3VlzCv90JC8/default.jpg",youtubeVedioList));
        playListModel.add(new PlayListModel("Odo Ahumaso"	,"PLyie0rQsQVfJu29kr4948ZAn0HCM6y7WC","https://i.ytimg.com/vi/qvCTkq21M-c/default.jpg",youtubeVedioList));
        playListModel.add(new PlayListModel("Me wo case anaa"	,"PLyie0rQsQVfKe9Qymdf5x-CCXkBJgchgn","https://i.ytimg.com/vi/1hgzqODM_C4/default.jpg",youtubeVedioList));
        playListModel.add(new PlayListModel("Asumasem"	,"PLyie0rQsQVfIJ0EBL7v_QzM9ot3IJiKXx","https://i.ytimg.com/vi/7aAKOm_OaBc/default.jpg",youtubeVedioList));
        playListModel.add(new PlayListModel("Pampaso"	,"PLyie0rQsQVfIl3Or6t82NxtYugNa91wCu","https://i.ytimg.com/vi/sWvVgJE6qeE/default.jpg",youtubeVedioList));
        playListModel.add(new PlayListModel("Nsem Nketenkete"	,"PLyie0rQsQVfKuIvpPLtcjE_7IiaLkPCra","https://i.ytimg.com/vi/3mF-TPuDrgY/default.jpg",youtubeVedioList));
        playListModel.add(new PlayListModel("Ahosepe"	,"PLyie0rQsQVfL4bk2J9TB1i-RnUfBPCeVw","https://i.ytimg.com/vi/vZ5yLyUPAtA/default.jpg",youtubeVedioList));
        playListModelList.addAll(playListModel);
    }
}
