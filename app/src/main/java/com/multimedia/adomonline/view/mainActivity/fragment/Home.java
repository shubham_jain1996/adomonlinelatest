package com.multimedia.adomonline.view.mainActivity.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.adapter.Slidingnews_Adapter;
import com.multimedia.adomonline.model.adapter.FragmentDataAdapter;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;
import com.multimedia.adomonline.model.modelClass.bannerData.Bannerdatamodel;
import com.multimedia.adomonline.model.modelClass.bannerData.HomeDataModel;
import com.multimedia.adomonline.model.modelClass.bannerData.HomeRecyclerModel;
import com.multimedia.adomonline.model.modelClass.player.SideMenuList;
import com.multimedia.adomonline.model.network.DataService;
import com.multimedia.adomonline.model.utility.ConvertHtmlToStringHome;
import com.multimedia.adomonline.model.utility.DataLoadingInterfaceHome;
import com.multimedia.adomonline.model.utility.InternetConnection;
import com.multimedia.adomonline.view.dialoge.ProgressDialogue;
import com.multimedia.adomonline.view.mainActivity.Article;
import com.multimedia.adomonline.view.mainActivity.MainActivity;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class Home extends Fragment implements DataLoadingInterfaceHome {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ViewPager mPager;
    LinearLayoutManager layoutManager;
    List<HomeClass> categoryList = new ArrayList<>();
    List<Bannerdatamodel> slidingBanner = new ArrayList<>();
    private Slidingnews_Adapter slidingNewAdapter;
    @BindView(R.id.topStories)
    RecyclerView topStoriesRecyclerView;
    @BindView(R.id.mainScrollView)
    NestedScrollView mainScrollView;


    @BindView(R.id.refreshPage)
    SwipeRefreshLayout refreshPage;

    private ProgressDialogue progressDialogue;


    private FragmentDataAdapter madapter;
    List<HomeRecyclerModel> recyclerviewData= new ArrayList<>();
    List<HomeRecyclerModel> homeDataModel = new ArrayList<>();
    private Dialog dialog;


    public static Home newInstance(String param1, String param2) {
        Home fragment = new Home();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view!=null){
            return view;
        }
         view= inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        progressDialogue= new ProgressDialogue(getActivity());
        progressDialogue.setCancelable(false);
        setUpSlidingNews(view);
        setUpTopStoriesnews();
        setUpModelClass();
        setUpRefreshLayout();
        checkInternetAndGetData();


        return view;
    }


    private void setUpRefreshLayout() {
        refreshPage.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recyclerviewData.clear();
                checkInternetAndGetData();
                if (refreshPage.isRefreshing()){
                    refreshPage.setRefreshing(false);
                }
            }
        });
    }

    private void checkInternetAndGetData() {
        if (InternetConnection.checkConnection(getActivity())) {
            getHomeData();
            progressDialogue.show();
            for ( HomeRecyclerModel item: homeDataModel) {
                getCategoryData(item);
            }
        }else{
            showInternetDialoge();
        }
    }

    private void showInternetDialoge() {
        if (dialog!=null && dialog.isShowing()){

                dialog.dismiss();

        }
        dialog = new Dialog(getActivity(),android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.no_internet);
        TextView retry = (TextView) dialog.findViewById(R.id.noInternet);
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkInternetAndGetData();


            }
        });
        dialog.show();
    }

    private void setUpModelClass() {
        homeDataModel.add(new HomeRecyclerModel("Top Stories","50"));
        homeDataModel.add(new HomeRecyclerModel("Politics","40"));
        homeDataModel.add(new HomeRecyclerModel("News","32"));
        homeDataModel.add(new HomeRecyclerModel("Entertainment","19"));
        homeDataModel.add(new HomeRecyclerModel("Sports","46"));
        homeDataModel.add(new HomeRecyclerModel("Business","18"));
        homeDataModel.add(new HomeRecyclerModel("Opinion","37"));
    }

    private void setUpTopStoriesnews() {
        topStoriesRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager verticalLayoutManager = new GridLayoutManager(getActivity(), 1);
        topStoriesRecyclerView.setLayoutManager(verticalLayoutManager);
        madapter = new FragmentDataAdapter(getActivity(),homeDataModel);
        topStoriesRecyclerView.setAdapter(madapter);

    }

    private void setUpSlidingNews(View view) {
        mPager = view.findViewById(R.id.newsPager);
        slidingNewAdapter = new Slidingnews_Adapter(getActivity(), slidingBanner, categoryList);
        mPager.setAdapter(slidingNewAdapter);
        mPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                page.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MainActivity.adCounter+=1;
                        Intent i = new Intent(getActivity(),Article.class);
                        i.putExtra("categoryType","Latest news");
                        i.putExtra("model", categoryList.get(mPager.getCurrentItem()));
                        i.putParcelableArrayListExtra("data", (ArrayList<? extends Parcelable>) categoryList);
                        i.putExtra("position",  mPager.getCurrentItem());
                        startActivity(i);
                    }
                });

            }
        });
    }



    public void getCategoryData(HomeRecyclerModel homeData) {
        DataService dataService = new DataService();
        dataService.setOnServerListener(new DataService.ServerResponseListner<List<HomeClass>>() {
            @Override
            public void onDataSuccess(List<HomeClass> response, int code) {

                if (response != null) {
                    new ConvertHtmlToStringHome(Home.this).execute(new HomeRecyclerModel(homeData.getCategory_Name(),homeData.getCategory_Id(),response));

                }
            }

            @Override
            public void onDataFailiure() {
            }
        });
        dataService.getTopStoriesData(homeData.getCategory_Id(),"1");
    }



    public void getHomeData() {
        DataService dataService = new DataService();
        dataService.setOnServerListener(new DataService.ServerResponseListner<List<HomeClass>>() {
            @Override
            public void onDataSuccess(List<HomeClass> response, int code) {
                progressDialogue.dismiss();
                 if (response != null) {
                    if (code == 200) {
                        categoryList.clear();
                        categoryList.addAll(response);
                        slidingNewAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onDataFailiure() {
            }
        });
        dataService.getHome();
    }

    @Override
    public void preExecute() {

    }

    @Override
    public void postExecute(HomeRecyclerModel homeRecyclerModel) {
        /*if (homeRecyclerModel.getCategory_Id().equals("50")) {
            recyclerviewData.add(0, homeRecyclerModel);
        } else {
            recyclerviewData.add(homeRecyclerModel);
        }*/
        for ( HomeRecyclerModel item: homeDataModel) {
            if (item.getCategory_Id().equals(homeRecyclerModel.getCategory_Id())){
                item.setHomeData(homeRecyclerModel.getHomedata());
                break;
            }
        }
        madapter.notifyDataSetChanged();
    }


}


