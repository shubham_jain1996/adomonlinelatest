package com.multimedia.adomonline.view.mainActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.dynamic.IFragmentWrapper;
import com.multimedia.adomonline.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Vuukel extends AppCompatActivity {
    public WebView popup;
    String articleid;
    @BindView(R.id.activity_main_webview_comments)
    WebView mWebViewComments;

    @BindView(R.id.container)
    RelativeLayout mContainer;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progress_view)
    CircularProgressView prograssBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vuukel);
        ButterKnife.bind(this);
        articleid=getIntent().getStringExtra("articleId");
        setUpToolbar(toolbar);
        configWebView();
       // mWebViewComments.loadUrl("https://cdn.vuukle.com/amp.html?apiKey=255be353-1ccf-4eea-af0c-86d618375550&host=adomonline.com&id="+articleid+"&img=&title=Newpost&url=");
        mWebViewComments.loadUrl("https://cdn.vuukle.com/amp.html?apiKey=255be353-1ccf-4eea-af0c-86d618375550&host=adomonline.com&id="+articleid+"&img=&title=Newpost&url=");

    }

    private void setUpToolbar(Toolbar toolbar) {
        AppCompatImageButton backPressed= toolbar.findViewById(R.id.backPressed);
        AppCompatImageButton refresh= toolbar.findViewById(R.id.refreshPage);
        backPressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWebViewComments.reload();
            }
        });

    }


    @Override
    public void onBackPressed() {
        if (this.popup == null || this.popup.getParent() == null) {
            mWebViewComments.goBack();
            super.onBackPressed();
            return;
        }
        mContainer.removeView(this.popup);
        popup = null;
        mWebViewComments.reload();

        super.onBackPressed();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void configWebView() {
        mWebViewComments.getSettings().setJavaScriptEnabled(true);
        mWebViewComments.getSettings().setDomStorageEnabled(true);
        mWebViewComments.getSettings().setSupportMultipleWindows(true);
        mWebViewComments.setWebChromeClient(new WebChromeClient() {
            private String link;

            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.d("consolejs", consoleMessage.message());
                if (consoleMessage.message().contains("Comments initialized!")) {
                    // mWebViewComments.loadUrl("javascript:signInUser('" + MainActivity.this.name + "', '" + MainActivity.this.email + "')");
                }
                return super.onConsoleMessage(consoleMessage);
            }

            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                Log.d("abc", "shouldOverrideUrlLoading: "+resultMsg);
                popup = new WebView(Vuukel.this);
                popup.getSettings().setJavaScriptEnabled(true);
                popup.getSettings().setPluginState(WebSettings.PluginState.ON);
                popup.getSettings().setSupportMultipleWindows(true);
                popup.setLayoutParams(view.getLayoutParams());
                popup.getSettings().setUserAgentString(popup.getSettings().getUserAgentString().replace("; wv", ""));
                popup.setWebViewClient(new WebViewClient() {
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {

                        if (url.contains("login")){

                            return true;
                        }else {
                            prograssBar.setVisibility(View.VISIBLE);
                            view.stopLoading();
                            Uri uri = Uri.parse(url);
                            String id = uri.getQueryParameter("id");
                            if (id == null) {
                                String adomLink = uri.getQueryParameter("url");
                                Uri uri1 = Uri.parse(adomLink);
                                link = uri1.getLastPathSegment();
                            }

                            if (id != null) {
                                Intent i = new Intent();
                                i.putExtra("isSlug", false);
                                i.putExtra("id", id);
                                setResult(RESULT_OK, i);
                                finish();
                            } else if (link != null) {
                                Intent i = new Intent();
                                i.putExtra("isSlug", true);
                                i.putExtra("id", link);
                                setResult(RESULT_OK, i);
                                finish();
                            }
                            return false;
                        }
                    }
                });
                popup.setWebChromeClient(new WebChromeClient() {
                    public void onCloseWindow(WebView window) {
                        mContainer.removeView(window);
                    }
                });
                // mContainer.addView(popup);
                ((WebView.WebViewTransport) resultMsg.obj).setWebView(popup);
                resultMsg.sendToTarget();
                return true;
            }

        });
        mWebViewComments.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                prograssBar.setVisibility(View.VISIBLE);
                if (url.contains("login")){
                    return true;
                }else {
                    view.stopLoading();
                    Uri uri = Uri.parse(url);
                    String id = uri.getQueryParameter("id");
                    String link = null;
                    if (id == null) {
                        String adomLink = uri.getQueryParameter("url");
                        Uri uri1 = Uri.parse(adomLink);
                        link = uri1.getLastPathSegment();
                    }

                    if (id != null) {
                        Intent i = new Intent();
                        i.putExtra("isSlug", false);
                        i.putExtra("id", id);
                        setResult(RESULT_OK, i);
                        finish();
                    } else if (link != null) {
                        Intent i = new Intent();
                        i.putExtra("isSlug", true);
                        i.putExtra("id", link);
                        setResult(RESULT_OK, i);
                        finish();
                    }
                    return false;
                }
            }
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                prograssBar.setVisibility(View.VISIBLE);
            }

            public void onPageFinished(WebView view, String url) {
                prograssBar.setVisibility(View.GONE);
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                prograssBar.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
               /* final AlertDialog.Builder builder = new AlertDialog.Builder(Vuukel.this);
                String message = "SSL Certificate error.";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }
                message += " Do you want to continue anyway?";

                builder.setTitle("SSL Certificate Error");
                builder.setMessage(message);
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
                dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.red));
                dialog.getButton(dialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.dark_green));*/
            }

        });
    }
}
