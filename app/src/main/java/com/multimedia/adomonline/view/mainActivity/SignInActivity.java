package com.multimedia.adomonline.view.mainActivity;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;


import com.multimedia.adomonline.R;
import com.multimedia.adomonline.view.mainActivity.fragment.registerFragment.SignIn;


public class SignInActivity extends AppCompatActivity {

    private FragmentManager manager;
    FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        initFragment(new SignIn());
    }
    private void initFragment(Fragment homeFragment) {
        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment, homeFragment);
        transaction.commit();
    }

    public void replaceFragmentWithBackStack(Fragment productScreen, String stack) {
        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment, productScreen);
        transaction.addToBackStack(stack);
        transaction.commit();
    }

}
