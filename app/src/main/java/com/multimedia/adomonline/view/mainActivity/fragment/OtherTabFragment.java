package com.multimedia.adomonline.view.mainActivity.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.adapter.CommonAdapter;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;
import com.multimedia.adomonline.model.network.DataService;
import com.multimedia.adomonline.model.utility.ConvertHtmlToString;
import com.multimedia.adomonline.model.utility.DataLoadingInterface;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OtherTabFragment extends Fragment implements DataLoadingInterface, CommonAdapter.ClickMoreButton {

    private String categoryName;
    private List<HomeClass> topStoriesList=new ArrayList<>();
    private int position1=0;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.refreshPage)
    SwipeRefreshLayout refreshPage;



    @BindView(R.id.progress_view)
    CircularProgressView prograssBar;

    private LinearLayoutManager layoutManager;
    private CommonAdapter madapter;
    private int pageNO=1;
    private  int id;
    private boolean _areLecturesLoaded=false;
    HomeClass homeModel;
    List<HomeClass> tempClass=new ArrayList<>();



    public static OtherTabFragment newInstance(String categoryName, int id) {
        OtherTabFragment fragment = new OtherTabFragment();

        Bundle args = new Bundle();
        args.putString("catName",categoryName);
        args.putInt("id",id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view!=null){
            return view;
        }
        Bundle bundle=getArguments();
        categoryName = bundle.getString("catName","");
        id= bundle.getInt("id");
        view = inflater.inflate(R.layout.fragment_temp, container, false);
        ButterKnife.bind(this,view);
        setUpSwipeRefresh();
        setUpRecyclerView();
        prograssBar.setVisibility(View.VISIBLE);

        getTopStoriesData(String.valueOf(id));
        return view;
    }

    private void setUpSwipeRefresh() {
        refreshPage.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNO=1;
                prograssBar.setVisibility(View.VISIBLE);
                getTopStoriesData(String.valueOf(id));
                if (refreshPage.isRefreshing()){
                    refreshPage.setRefreshing(false);
                }
            }
        });

    }

    private void setUpRecyclerView() {
        recyclerView.setVisibility(View.INVISIBLE);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager verticalLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(verticalLayoutManager);
        madapter = new CommonAdapter(getActivity(), topStoriesList,categoryName,this);
        recyclerView.setAdapter(madapter);
    }

    public void getTopStoriesData(String Id) {
        DataService dataService = new DataService();
        dataService.setOnServerListener(new DataService.ServerResponseListner<List<HomeClass>>() {
            @Override
            public void onDataSuccess(List<HomeClass> response, int code) {

                if (response != null) {

                    new ConvertHtmlToString(OtherTabFragment.this).execute(response);

                }else{
                    prograssBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onDataFailiure() {
            }
        });
        dataService.getallData(Id, String.valueOf(pageNO));
    }

    @Override
    public void preExecute() {
        prograssBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void postExecute(List<HomeClass> homeClasses) {
        if (pageNO==1){
            topStoriesList.clear();
        }
        topStoriesList.addAll(homeClasses);
        madapter.notifyDataSetChanged();
        prograssBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void clickMore() {
        pageNO++;
        prograssBar.setVisibility(View.VISIBLE);
        getTopStoriesData(String.valueOf(id));
    }

}

