package com.multimedia.adomonline.view.mainActivity.fragment;

import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.multimedia.adomonline.R;


public class Vedio extends Fragment implements YouTubePlayer.OnInitializedListener  {
    private YouTubePlayerView youTubeView;
    private String API_KEY="AIzaSyBwtbtvkmxDmso564A9I1TdFyQ4Tf2KkNY";
    private static final int RECOVERY_REQUEST = 1;
    String videoId,title,desc;
    AudioManager am = null;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view=inflater.inflate(R.layout.fragment_vedio, container, false);
        YouTubePlayerSupportFragment frag =
                (YouTubePlayerSupportFragment)getChildFragmentManager().findFragmentById(R.id.youtube_fragment);

        frag.initialize(API_KEY, this);
        return view;
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }
}
