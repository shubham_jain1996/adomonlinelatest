package com.multimedia.adomonline.view.mainActivity.media;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.text.style.TtsSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.network.DataService;
import com.multimedia.adomonline.model.tv.YoutubeChannelList;
import com.multimedia.adomonline.view.mainActivity.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LiveTv extends AppCompatActivity implements YouTubePlayer.OnInitializedListener {
    boolean isFullScreen=false;
    YouTubePlayer youTubePlayer;
    boolean wasRestored;
    private static final int RECOVERY_REQUEST = 1;
    boolean isAdomTV_Play=true;
    boolean isJoyTvPlay=false;

    @BindView(R.id.joyTv)
    CardView joyTvView;

    @BindView(R.id.adomTv)
    CardView adomTvView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private AdView mAdView;
    private InterstitialAd adView;
    String adomTvChannelId, myjoyTvChannelId;

    boolean isAdom=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_tv);
        ButterKnife.bind(this);
        setUpToolBar();
        loadAd();
        YouTubePlayerSupportFragment frag =(YouTubePlayerSupportFragment)getSupportFragmentManager().findFragmentById(R.id.youtube_fragment);
        frag.initialize(getResources().getString(R.string.YouTubekey), this);
        adView = new InterstitialAd(this);
        adView.setAdUnitId(getResources().getString(R.string.interId));
        setUpInterStialAd();
        getLiveTvChannelId();
        getMyJoyChannelId();


    }

    private void getLiveTvChannelId() {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<YoutubeChannelList>() {
            @Override
            public void onDataSuccess(YoutubeChannelList response, int code) {
                if (response.getItems().size()>0){
                    adomTvChannelId=response.getItems().get(0).getId().getVideoId();
                    playYouTubeVedio(adomTvChannelId);
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.getAdomTvChannelId();

    }

    private void getMyJoyChannelId() {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<YoutubeChannelList>() {
            @Override
            public void onDataSuccess(YoutubeChannelList response, int code) {
                if (response.getItems().size()>0){
                    myjoyTvChannelId=response.getItems().get(0).getId().getVideoId();
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.getMyJoyChannelId();

    }
    private void loadAd() {
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void setUpToolBar() {
        TextView title= toolbar.findViewById(R.id.title);
        title.setText("Live Tv");
        ImageView backPressed= toolbar.findViewById(R.id.backPressed);
        backPressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        this.youTubePlayer= youTubePlayer;
        this.wasRestored= wasRestored;

        youTubePlayer.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {
            @Override
            public void onFullscreen(boolean b) {
                isFullScreen=b;
            }
        });


    }
    public void showAdd()
    {
        if (adView.isLoaded()){
            adView.show();
        }
        MainActivity.adCounter++;
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = String.format("Error initializing YouTube player", errorReason.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }
    private void playYouTubeVedio(String channelId) {
        if (MainActivity.adCounter%5==0){
            showAdd();
        }
        if (isJoyTvPlay){
            adomTvView.setCardBackgroundColor(getResources().getColor(R.color.white));
            joyTvView.setCardBackgroundColor(getResources().getColor(R.color.orange));
        }else if (isAdomTV_Play){
            joyTvView.setCardBackgroundColor(getResources().getColor(R.color.white));
            adomTvView.setCardBackgroundColor(getResources().getColor(R.color.orange));
        }
        if ( channelId!=null && youTubePlayer!=null  && !wasRestored) {
            youTubePlayer.loadVideo(channelId);
            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
        }

    }

    @OnClick(R.id.adomTv)
    public void PlayAdomTv(){
        if (!isAdomTV_Play) {
            isJoyTvPlay=false;
            isAdomTV_Play=true;
          //  playYouTubeVedio("-738tYJqSdI");
            playYouTubeVedio(adomTvChannelId);
        }

    }

    @OnClick(R.id.joyTv)
    public void PlayJoyTv(){
        if (!isJoyTvPlay) {
            isJoyTvPlay=true;
            isAdomTV_Play=false;
           // playYouTubeVedio("ar0pKYiZ3h4");
            playYouTubeVedio(myjoyTvChannelId);
        }

    }
    private void setUpInterStialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
              /* if (adCounter%5==0) {
                   adView.show();
               }*/
            }
            @Override
            public void onAdFailedToLoad(int i) {
                Log.d("satyam", "onAdFailedToLoad: "+i);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (isFullScreen){
            youTubePlayer.setFullscreen(false);
        }else{
            super.onBackPressed();
        }


    }
}
