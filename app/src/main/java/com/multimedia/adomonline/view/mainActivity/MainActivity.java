package com.multimedia.adomonline.view.mainActivity;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.facebook.CallbackManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.adapter.NavigationAdapter;
import com.multimedia.adomonline.model.adapter.ViewPagerAdapter;
import com.multimedia.adomonline.model.modelClass.player.RadioModel;
import com.multimedia.adomonline.model.modelClass.player.SideMenuList;
import com.multimedia.adomonline.model.player.NotiService;
import com.multimedia.adomonline.model.player.PlayerNotification;
import com.multimedia.adomonline.model.player.SharedExoPlayer;
import com.multimedia.adomonline.model.utility.Application;
import com.multimedia.adomonline.model.utility.Constants;
import com.multimedia.adomonline.model.utility.InternetConnection;
import com.multimedia.adomonline.presenter.GetNavigationItemImpl;
import com.multimedia.adomonline.presenter.MainPresenter;
import com.multimedia.adomonline.presenter.MainPresenterImpl;
import com.multimedia.adomonline.presenter.MainView;
import com.multimedia.adomonline.view.dialoge.ProgressDialogue;
import com.multimedia.adomonline.view.mainActivity.media.LiveTv;
import com.multimedia.adomonline.view.mainActivity.media.LiveVideos;
import com.multimedia.adomonline.view.search.SearchActivity;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.multimedia.adomonline.view.mainActivity.PlayerActivity.songPositionclick;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,MainView {
    private int LOCATION_DATA=5;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private DrawerLayout drawer;
    RecyclerView navigationItem;
    LinearLayoutManager layoutManager;
    MainPresenter presenter;
    List<SideMenuList> categoryName = new ArrayList<>();
    private ViewPagerAdapter adapter;
    List<SideMenuList> sideMenuLists= new ArrayList<>();
    SideMenuList sideMenuModel;
    private NavigationAdapter navigationAdapter;
    private AdView mAdView;
    private InterstitialAd adView;
    ProgressDialogue progressDialogue;
    public static int adCounter=0;
    private boolean doubleBackToExitPressedOnce=false;

    @BindView(R.id.socialShare)
    AppCompatImageButton socialShare;

    List<RadioModel> radioData= new ArrayList<>();

    CallbackManager callbackManager;
    ShareDialog shareDialog;



    private int recyclerPosition;
    private SharedPreferences.Editor session;
    private List<RadioModel> channelList=new ArrayList<>();
    private String songUrl;
    private int songPosition;
    private IntentFilter filter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        setUpProgressbar();
        setUpModelData();


        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.navigation_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toolbar = findViewById(R.id.toolbar);
        tabLayout =  findViewById(R.id.tabs);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_24px);
        presenter = new MainPresenterImpl(this, new GetNavigationItemImpl());
        navigationItem = findViewById(R.id.navigationItem);
        navigationItem.setHasFixedSize(true);
       // setUpNavigationClick();
        layoutManager=new LinearLayoutManager(this);
        RecyclerView.LayoutManager lm = new GridLayoutManager(this, 1);
        navigationItem.setLayoutManager(lm);
        viewPager = findViewById(R.id.viewpager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        adapter = new ViewPagerAdapter(getSupportFragmentManager(),categoryName);
        viewPager.setAdapter(adapter);
        presenter.GetData();
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position1) {
                adCounter++;
                if (adCounter%5==0){
                    showAdd();
                }
                sideMenuModel.setisSelected(false);
                sideMenuModel = sideMenuLists.get(position1);
                sideMenuModel.setisSelected(true);
                navigationAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        adView = new InterstitialAd(this);
        adView.setAdUnitId(getResources().getString(R.string.interId));
        setUpInterStialAd();

         filter = new IntentFilter();
        filter.addAction("play");
        filter.addAction("rewind");
        filter.addAction("forword");

    }

    private void setUpProgressbar() {
        progressDialogue= new ProgressDialogue(this);
        progressDialogue.setCancelable(false);

    }

    public void showAdd()
    {
        if (adView.isLoaded()){
            adView.show();
        }
    }
    private void setUpInterStialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
              /* if (adCounter%5==0) {
                   adView.show();
               }*/
            }
            @Override
            public void onAdFailedToLoad(int i) {
                Log.d("satyam", "onAdFailedToLoad: "+i);
            }
        });
    }

    public   void setUpNavigationClick(int position) {

                if (sideMenuLists.get(position).getExpandable()){

                   if (sideMenuLists.get(position).getExpandableClick()){
                       sideMenuLists.get(position).setExpandableClick(false);
                    }else{
                       sideMenuLists.get(position).setExpandableClick(true);
                    }
                    navigationAdapter.notifyDataSetChanged();
                }else {
                    drawer.closeDrawers();
                    sideMenuModel.setisSelected(false);
                    sideMenuModel.setExpandableClick(false);
                    sideMenuModel = sideMenuLists.get(position);
                    sideMenuModel.setisSelected(true);
                    navigationAdapter.notifyDataSetChanged();
                    viewPager.setCurrentItem(position, true);
                }

    }

    public   void setUpNavigationClickForChild(int position,int childPosition) {
        if (sideMenuLists.get(position).getSubmenu().get(childPosition).getId()==0){
            if (broadcastReceiver!=null){
                unregisterReceiver(broadcastReceiver);
            }
            drawer.closeDrawers();
            adCounter++;
            showAdd();
            Intent i = new Intent(MainActivity.this,PlayerActivity.class);
            startActivity(i);

        }
        else if (sideMenuLists.get(position).getSubmenu().get(childPosition).getId()==1)
        {
            drawer.closeDrawers();
            adCounter++;
            showAdd();
            Intent i = new Intent(MainActivity.this, LiveTv.class);
            startActivity(i);

        }
        else if (sideMenuLists.get(position).getSubmenu().get(childPosition).getId()==2)

        {
            drawer.closeDrawers();
            adCounter++;
            showAdd();
            Intent i = new Intent(MainActivity.this, LiveVideos.class);
            startActivity(i);

        }


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem)
    {
        return false;
    }

    @Override
    public void showProgress() {
        if (progressDialogue!=null){
            if (progressDialogue.isShowing()){
                progressDialogue.dismiss();
            }
                progressDialogue.show();
        }

    }

    @Override
    public void hideProgress() {
        if (progressDialogue!=null){
            if (progressDialogue.isShowing()){
                progressDialogue.dismiss();
            }
        }
    }

    @Override
    public void setSideMenuList(List<SideMenuList> sideMenuList) {
        sideMenuLists.addAll(sideMenuList);
        sideMenuModel=sideMenuList.get(0);
        sideMenuModel.setisSelected(true);
        navigationAdapter=new NavigationAdapter(this,sideMenuLists);
        navigationItem.setAdapter(navigationAdapter);
        sideMenuList.remove(sideMenuList.size()-1);
        categoryName.addAll(sideMenuList);
        adapter.notifyDataSetChanged();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolsearch, menu);
        MenuItem searchItem=menu.findItem(R.id.search);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent i  = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(i);
               // startActivityForResult(i,LOCATION_DATA);
                return false;

            }
        });
      /*  MenuItem searchViewItem = menu.findItem(R.id.search);
        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) searchViewItem.getActionView();
        searchView.setQueryHint("Search for Product,Brands...");
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);// Do not iconify the widget; expand it by defaul

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                // This is your adapter that will be filtered


                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                // **Here you can get the value "query" which is entered in the search box.**



                return true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);*/
        return super.onCreateOptionsMenu(menu);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
      /*  if (item.getItemId() == R.id.search) {

            return true;
        }else*/ if (item.getItemId() == android.R.id.home){
            drawer.openDrawer(Gravity.LEFT);
        }



        return super.onOptionsItemSelected(item);
    }

@OnClick(R.id.socialShare)
    public void shareApp(){
    socialShare.setEnabled(false);
      showProgress();
    shareIntent();
}

    @OnClick(R.id.twiterShare)
    public void shareAppOnTwiter(){
        showProgress();
        shareTwitter("Adom Online&url=https://play.google.com/store/apps/details?id=com.multimedia.adomonline");
    }
    @OnClick(R.id.whatsappShare)
    public void shareAppOnWhatsapp(){
        showProgress();
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, "The text you wanted to share");
        try {
            hideProgress();
            startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            hideProgress();
            Toast.makeText(this, "Whatsapp have not been installed.", Toast.LENGTH_SHORT).show();

        }
    }

    @OnClick(R.id.facebookShare)
    public void openFaceBookShare(){
         ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.multimedia.adomonline"))
                .setQuote("AdomOnline")
                 .build();
         shareDialog.show(content);
    }


    private void shareTwitter(String message) {
        Intent tweetIntent = new Intent(Intent.ACTION_SEND);
        tweetIntent.putExtra(Intent.EXTRA_TEXT, message);
        tweetIntent.setType("text/plain");

        PackageManager packManager = getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;
        for (ResolveInfo resolveInfo : resolvedInfoList) {
            if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                tweetIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name);
                resolved = true;
                break;
            }
        }
        if (resolved) {
            hideProgress();
            startActivity(tweetIntent);
        } else {
            Intent i = new Intent();
            i.putExtra(Intent.EXTRA_TEXT, message);
            i.setAction(Intent.ACTION_VIEW);
            i.setData(Uri.parse("https://twitter.com/intent/tweet?text=" + urlEncode(message)));
            hideProgress();
            startActivity(i);
            Toast.makeText(this, "Twitter app isn’t found", Toast.LENGTH_LONG).show();
        }
    }

    private String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {

            return "";
        }
    }
    private void shareIntent() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "AdomOnline");
            String shareMessage= "\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=com.multimedia.adomonline";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            hideProgress();
            startActivity(Intent.createChooser(shareIntent, "choose one"));
            socialShare.setEnabled(true);
        } catch(Exception e) {
            socialShare.setEnabled(true);
            hideProgress();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, filter);

    }

    @Override
    public void onBackPressed() {
        if (viewPager!=null){
            if (viewPager.getCurrentItem()==0){
                doubleClickToExit();
            }else {
                viewPager.setCurrentItem(0);
            }
        }else{
            doubleClickToExit();
        }

    }

    public void doubleClickToExit(){
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("shubham", "onReceive: "+intent.getAction());
            switch (intent.getAction()){
                case "play":
                    controlPlayer();
                    break;

                case "rewind":
                    priviousChannel();
                    break;

                case "forword":
                    forwordChannel();
                    break;
            }
        }
    };

    public void controlPlayer(){
        boolean playerState;
        playerState = SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady();
        if(playerState)
        {
            SharedExoPlayer.getInstance().pauseRadio();
        }
        else
        {
            SharedExoPlayer.getInstance().checkAudio();
        }

    }

    public void priviousChannel(){
        if (songPositionclick<=0){
            songPositionclick=radioData.size()-1;

            playExoPlayerRadio(songPositionclick,radioData);
        }else{
            songPositionclick--;
            if (songPositionclick>=0 && songPositionclick<radioData.size()){
                playExoPlayerRadio(songPositionclick,radioData);

            }
        }
        saveStateInSession(songPositionclick);

    }

    public void forwordChannel(){
        if (songPositionclick>=radioData.size()-1){
            songPositionclick=0;
            playExoPlayerRadio(songPositionclick,radioData);

        }else{
            songPositionclick++;
            if (songPositionclick<radioData.size()){
                playExoPlayerRadio(songPositionclick,radioData);

            }
        }
        saveStateInSession(songPositionclick);

    }



    public void playExoPlayerRadio( int position,List<RadioModel> radioData){
        this.channelList=radioData;
        SharedExoPlayer.getInstance().PlayAudio(radioData.get(position).getSourceUrl());
        this.songUrl=radioData.get(position).getSourceUrl();
        songPosition=position;
        playMusic();
    }
    private void playMusic() {

        if(InternetConnection.checkConnection(this)) {
            if (SharedExoPlayer.getInstance().exoPlayer != null) {
                this.startService(new Intent(this, NotiService.class));
                if (SharedExoPlayer.getInstance().exoPlayer.getPlaybackState() == Player.STATE_READY) {
                    boolean playerState = SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady();
                } else if (SharedExoPlayer.getInstance().exoPlayer.getPlaybackState() == Player.STATE_BUFFERING) {
                }
                else {
                    SharedExoPlayer.getInstance().PlayAudio(songUrl);

                }
            } else {
                SharedExoPlayer.getInstance().PlayAudio(songUrl);

            }
        } else {
            InternetConnection.AlertBox(this, "Please Check Your Internet Connection");

        }


    }

    private void setUpModelData() {
        radioData.add(new RadioModel(this.getResources().getString(R.string.adom_title),this.getResources().getString(R.string.adom_tagline),"http://mmg.streamguys1.com/AdomFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.adom_fm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.asempa_title),this.getResources().getString(R.string.asempa_tagline),"http://mmg.streamguys1.com/AsempaFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.asempa_fm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.hitz_title),this.getResources().getString(R.string.hitz_tagline),"http://mmg.streamguys1.com/HitzFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.hitz_fm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.joy_title),this.getResources().getString(R.string.joy_tagline),"http://mmg.streamguys1.com/JoyFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.ic_joyfm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.luv_title),this.getResources().getString(R.string.luv_tagline),"http://mmg.streamguys1.com/LuvFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.luv_fm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.nhyira_title),this.getResources().getString(R.string.nhyira_tagline),"http://mmg.streamguys1.com/NhyiraFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.nhyira_fm,false));

    }


    public void saveStateInSession(int position){
        session= Application.shardPref.edit();
        session.putInt(Constants.Pref.PLAYERSTATE,position );
        session.apply();
    }

    @OnClick(R.id.watchTv)
    public void watchTv(){
        adCounter++;
        showAdd();
        Intent i = new Intent(MainActivity.this, LiveTv.class);
        startActivity(i);
    }

    @OnClick(R.id.playRadio)
    public void playRadio(){
        if (broadcastReceiver!=null){
            unregisterReceiver(broadcastReceiver);
        }
        adCounter++;
        showAdd();
        Intent i = new Intent(MainActivity.this, PlayerActivity.class);
        startActivity(i);
    }

    @Override
    protected void onDestroy() {
        if (broadcastReceiver!=null){
            unregisterReceiver(broadcastReceiver);
        }
        super.onDestroy();
    }
  /*  @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 5) {
                Intent i = new Intent(MainActivity.this,Article.class);
                i.putExtra("categoryType",data.getExtras().getString("categoryType"));
                i.putExtra("model",   data.getExtras().getParcelableArray("model"));
                i.putExtra("position", data.getExtras().getInt("position"));
                i.putParcelableArrayListExtra("data",data.getExtras().getParcelableArrayList("data"));
                startActivity(i);
            }

        }
    }*/

}
