package com.multimedia.adomonline.model.adapter.searchAdapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;

import java.util.ArrayList;
import java.util.List;


public class SearchItemAdapter extends RecyclerView.Adapter<SearchItemAdapter.ViewHolder> {
    private Context context;
    List<HomeClass> locationData = new ArrayList<>();
    int pos;


    public SearchItemAdapter(Context context , List<HomeClass> locationData) {
        this.context = context;
        this.locationData=locationData;


    }



    @Override
    public SearchItemAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.searchitem_adapter, viewGroup, false);
        return new SearchItemAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(SearchItemAdapter.ViewHolder viewHolder, final int position) {
       viewHolder.locationName.setText(Html.fromHtml(locationData.get(position).getTitle().getRendered()));
        viewHolder.locationAddress.setText(Html.fromHtml(locationData.get(position).getExcerpt().getRendered()));
    }
    @Override
    public int getItemCount() {
        if (locationData.size()>0) {
            return locationData.size();
        }else {
            return 0;
        }
    }

     static class ViewHolder extends RecyclerView.ViewHolder {

       TextView locationName, locationAddress;


         ViewHolder(View view) {
            super(view);

             locationName=view.findViewById(R.id.locationName);
             locationAddress=view.findViewById(R.id.locationAddress);


        }
    }

    public  int getPos(){

        return pos;
    }


}