package com.multimedia.adomonline.model.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.modelClass.player.RadioModel;

import java.util.ArrayList;
import java.util.List;


public class RadioAdapter extends RecyclerView.Adapter<RadioAdapter.ViewHolder> {
    private Context context;
    List<RadioModel> data= new ArrayList<>();


    public RadioAdapter(Context context, List<RadioModel> data) {
        this.context = context;

        this.data=data;

    }


    @Override
    public RadioAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.radio_adapter, viewGroup, false);
        return new RadioAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RadioAdapter.ViewHolder viewHolder, int position) {
        viewHolder.fmImage.setImageResource(data.get(position).getIcon());
        viewHolder.fmName.setText(data.get(position).getTitle());
        viewHolder.tagLine.setText(data.get(position).getTagLine());
        if (data.get(position).getIsPlay()){
            viewHolder.play.setVisibility(View.VISIBLE);
        }else {
            viewHolder.play.setVisibility(View.INVISIBLE);
        }

    }
    @Override
    public int getItemCount() {
        return data.size();
    }

     static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView fmImage;
        TextView fmName, tagLine,play;
         ViewHolder(View view) {
            super(view);
            fmImage=view.findViewById(R.id.fm_image);
             fmName=view.findViewById(R.id.channelName);
             tagLine=view.findViewById(R.id.tagLine);
             play=view.findViewById(R.id.play);


        }
    }
}