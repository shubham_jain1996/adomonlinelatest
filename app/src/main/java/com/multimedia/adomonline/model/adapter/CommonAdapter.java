package com.multimedia.adomonline.model.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Parcelable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;
import com.multimedia.adomonline.model.modelClass.bannerData.Bannerdatamodel;
import com.multimedia.adomonline.model.network.DataService;
import com.multimedia.adomonline.model.util.Util;
import com.multimedia.adomonline.view.mainActivity.Article;
import com.multimedia.adomonline.view.mainActivity.Details;
import com.multimedia.adomonline.view.mainActivity.MainActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CommonAdapter extends RecyclerView.Adapter {


    private final int HEADER = 0;
    private final int FOOTER = 1;
    private final int VIEW_TYPE_ITEM = 2;

    Context context;
    List<HomeClass> allData = new ArrayList<>();
    List<HomeClass> relatedArticles = new ArrayList<>();

    String categoryName;
   ClickMoreButton clickMoreButton;
    private int loop;

    public CommonAdapter(Context context, List<HomeClass> allData, String categoryName,ClickMoreButton callBack) {
        this.context = context;
        this.allData = allData;
        this.categoryName = categoryName;
        this.clickMoreButton = callBack;

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header, parent, false);
            return new ItemViewHolder(view);
        } else if (viewType == FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_button, parent, false);
            return new ItemViewHolder1(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv1item, parent, false);
            return new ItemViewHolder2(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ItemViewHolder) {
            populateItemRows((ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof ItemViewHolder1) {
            ItemViewHolder1 itemViewHolder = (ItemViewHolder1) viewHolder;
            populateItemRows1((ItemViewHolder1) viewHolder, position);
            if (allData.size()%20!=0){
                itemViewHolder.moreStories.setVisibility(View.GONE);
            }else{
                itemViewHolder.moreStories.setVisibility(View.VISIBLE);
            }
            itemViewHolder.moreStories.setOnClickListener(new ViewMoreAction());

        } else if (viewHolder instanceof ItemViewHolder2) {
           ItemViewHolder2 itemViewHolder = (ItemViewHolder2) viewHolder;
            populateItemRows2((ItemViewHolder2) viewHolder, position);
        }
    }

    class ViewMoreAction implements View.OnClickListener {

        @Override
        public void onClick(View v) {

            clickMoreButton.clickMore();
        }
    }

    @Override
    public int getItemCount() {
        return allData.size() + 2;
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER;
        } else if (position == allData.size() + 1) {
            return FOOTER;
        }
        return VIEW_TYPE_ITEM;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView text;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text);

        }
    }

    private class ItemViewHolder1 extends RecyclerView.ViewHolder {
        Button moreStories;

        public ItemViewHolder1(@NonNull View itemView) {
            super(itemView);
            moreStories = itemView.findViewById(R.id.moreStories);
        }
    }

    private class ItemViewHolder2 extends RecyclerView.ViewHolder {
        ImageView brandPic;
        TextView text, text2, newsTime, newsType;
        ProgressBar progressBar;

        public ItemViewHolder2(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.prograssIndicator);
            brandPic = itemView.findViewById(R.id.image1);
            text = itemView.findViewById(R.id.text1);
            text2 = itemView.findViewById(R.id.text2);
            newsTime = itemView.findViewById(R.id.newsTime);
            newsType = itemView.findViewById(R.id.newsType);

        }
    }

    private void populateItemRows(ItemViewHolder viewHolder, int i) {
        viewHolder.text.setText(categoryName);

    }

    private void populateItemRows1(ItemViewHolder1 viewHolder, int position) {

    }

    private void populateItemRows2(ItemViewHolder2 viewHolder, int position) {
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    assignArray();
                MainActivity.adCounter +=1;
                Intent i = new Intent(context, Article.class);
                i.putExtra("model", allData.get(position - 1));
                i.putExtra("categoryType", categoryName);
                i.putExtra("position",  position);
                i.putParcelableArrayListExtra("data", (ArrayList<? extends Parcelable>) relatedArticles);
                context.startActivity(i);
            }
        });
        HomeClass homeClass = allData.get(position - 1);
        viewHolder.text.setText(homeClass.getTitle().getRendered());
        viewHolder.text2.setText(homeClass.getExcerpt().getRendered());
        viewHolder.newsType.setText(categoryName);
        viewHolder.brandPic.setImageResource(R.drawable.new_adom_logo);
        if (homeClass.getMedia_url() != null) {
            Picasso.get().load(homeClass.getMedia_url()).placeholder(R.drawable.new_adom_logo).into(viewHolder.brandPic);
        } else
            {

            downLoadImages(viewHolder.brandPic, homeClass);
        }

        viewHolder.newsTime.setText(Util.parseDatefromServer(homeClass.getDate()));
    }

    private void assignArray() {
        if (relatedArticles.size()>0){
            relatedArticles.clear();
        }
        if (allData.size()>0 && allData.size()<=4){
            loop=allData.size();

        }else if (allData.size()>4){
            loop=5;
        }
        if (loop>0) {
            for (int i = 0; i < loop; i++) {
                relatedArticles.add(allData.get(i));
            }
        }
    }

    public void downLoadImages(final ImageView imageView, final HomeClass data) {
        DataService dataService = new DataService();
        dataService.setOnServerListener(new DataService.ServerResponseListner<Bannerdatamodel>() {
            @Override
            public void onDataSuccess(Bannerdatamodel response, int code) {
                if (code == 200) {
                    if (response != null) {
                        try {
                            if (response.getMediaDetails().getSizes().getMedium() != null) {
                                data.setMedia_url(response.getMediaDetails().getSizes().getMedium().getSourceUrl());
                            }
                            else if (response.getMediaDetails().getSizes().getThumbnail() != null)
                            {
                                data.setMedia_url(response.getMediaDetails().getSizes().getThumbnail().getSourceUrl());
                            }
                            else
                                {
                                data.setMedia_url(response.getSourceUrl());
                                }
                            data.setSource_url(response.getSourceUrl());
                            Picasso.get().load(data.getMedia_url()).placeholder(R.drawable.new_adom_logo).into(imageView);
                        } catch (Exception e) {
                            imageView.setImageResource(R.drawable.new_adom_logo);

                        }


                    }
                }else{
                    imageView.setImageResource(R.drawable.new_adom_logo);
                }

            }

            @Override
            public void onDataFailiure() {
            }
        });
        dataService.getBannerData(String.valueOf(data.getFeaturedMedia()));

    }

    public interface ClickMoreButton {
        void clickMore();
    }
}

