package com.multimedia.adomonline.model.modelClass.bannerData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;

import java.util.ArrayList;
import java.util.List;

public class HomeDataModel {
    @SerializedName("category_Name")
    @Expose
    private String category_Name;
    @SerializedName("category_Id")
    @Expose
    private String category_Id;

    public List<HomeClass> getTopStories() {
        return topStories;
    }

    public void setTopStories(List<HomeClass> topStories) {
        this.topStories = topStories;
    }

    private List<HomeClass> topStories =new ArrayList<HomeClass>();
    public HomeDataModel() {
    }

    public HomeDataModel(String category_Name, String category_Id) {
        this.category_Name = category_Name;
        this.category_Id = category_Id;
    }

    public String getCategory_Name() {
        return category_Name;
    }

    public void setCategory_Name(String category_Name) {
        this.category_Name = category_Name;
    }

    public String getCategory_Id() {
        return category_Id;
    }

    public void setCategory_Id(String category_Id) {
        this.category_Id = category_Id;
    }


}
