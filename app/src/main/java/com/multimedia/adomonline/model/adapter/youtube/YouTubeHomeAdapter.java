package com.multimedia.adomonline.model.adapter.youtube;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.modelClass.youtubeFiles.YoutubeVedioList;
import com.multimedia.adomonline.model.util.Util;
import com.multimedia.adomonline.view.mainActivity.MainActivity;
import com.multimedia.adomonline.view.mainActivity.media.LiveVideos;
import com.multimedia.adomonline.view.mainActivity.media.YouTubeVideos;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


public class YouTubeHomeAdapter extends RecyclerView.Adapter<YouTubeHomeAdapter.ViewHolder> {
    private Context context;
    YoutubeVedioList data;


    public YouTubeHomeAdapter(Context context, YoutubeVedioList data) {
        this.context = context;
        this.data=data;

    }


    @Override
    public YouTubeHomeAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.youtube_home_adapter, viewGroup, false);
        return new YouTubeHomeAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(YouTubeHomeAdapter.ViewHolder viewHolder, int position) {
        Picasso.get().load("https://i.ytimg.com/vi/"+data.getItems().get(position).getSnippet().getResourceId().getVideoId()+"/mqdefault.jpg").error(R.drawable.new_adom_logo).into(viewHolder.vedioImage, new Callback() {
            @Override
            public void onSuccess() {
                viewHolder.progress_view.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                viewHolder.progress_view.setVisibility(View.GONE);
            }
        });
        viewHolder.title.setText(data.getItems().get(position).getSnippet().getTitle());
        viewHolder.vedioTime.setText(Util.parseDatefromServer(data.getItems().get(position).getSnippet().getPublishedAt()));
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.adCounter+=1;
                if (MainActivity.adCounter%5==0){
                    ((LiveVideos)context).showAdd();
                }
                Intent i = new Intent(context, YouTubeVideos.class);
                i.putExtra("listData",  data);
                i.putExtra("playlistId",data.getItems().get(position).getSnippet().getPlaylistId());
                i.putExtra("position",position);
                i.putExtra("type","inter");
                context.startActivity(i);
            }
        });

    }
    @Override
    public int getItemCount() {
        if (data!=null){
            return data.getItems().size();
        }else{
            return 0;
        }

    }

     static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView vedioImage;
        TextView title,vedioTime;
        CircularProgressView progress_view;
         ViewHolder(View view) {
            super(view);
             progress_view=view.findViewById(R.id.progress_view);
             vedioImage=view.findViewById(R.id.vedioImage);
             title=view.findViewById(R.id.title);
             vedioTime=view.findViewById(R.id.vedioTime);



        }
    }
}