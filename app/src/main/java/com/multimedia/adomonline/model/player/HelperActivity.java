package com.multimedia.adomonline.model.player;

import android.app.Activity;
import android.os.Bundle;

public class HelperActivity extends Activity {

    private HelperActivity ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = this;
        String action = (String) getIntent().getExtras().get("DO");
        if (action.equals("play")) {
            boolean playerState = SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady();
            if(playerState)
            {
                SharedExoPlayer.getInstance().pauseRadio();
            }
            else
            {
                SharedExoPlayer.getInstance().checkAudio();
            }
        } else if (action.equals("next")) {
            
        } else if (action.equals("back")) {
            
        } 

        if (!action.equals("reboot"))
            finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}