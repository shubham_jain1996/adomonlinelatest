package com.multimedia.adomonline.model.utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.ads.MobileAds;
import com.multimedia.adomonline.R;

public class Application extends android.app.Application{
    public static SharedPreferences shardPref;
    private static Context mContext;


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        shardPref = getSharedPreferences(Constants.PREF_NAME,MODE_PRIVATE);
        MobileAds.initialize(this, this.getResources().getString(R.string.appId));


    }
    public static Context getContext() {
        return mContext;
    }
}
