package com.multimedia.adomonline.model.modelClass.player;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubMenuList {
    @SerializedName("Title")
    @Expose
    private String Title;


    @SerializedName("Id")
    @Expose
    private Integer id;



    public SubMenuList(String title, Integer id) {
        Title = title;
        this.id = id;


    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public SubMenuList() {
    }


    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }







}
