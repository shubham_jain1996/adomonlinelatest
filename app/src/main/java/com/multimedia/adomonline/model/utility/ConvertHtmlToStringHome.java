package com.multimedia.adomonline.model.utility;

import android.os.AsyncTask;
import android.text.Html;

import com.multimedia.adomonline.model.modelClass.bannerData.HomeRecyclerModel;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;

import java.util.List;

public class ConvertHtmlToStringHome extends AsyncTask<HomeRecyclerModel, String, HomeRecyclerModel> {
  private DataLoadingInterfaceHome callBack;
  public ConvertHtmlToStringHome(DataLoadingInterfaceHome dataLoading){
      this.callBack = dataLoading;
  }
  @Override
  protected void onPostExecute(HomeRecyclerModel homeClasses) {
      callBack.postExecute(homeClasses);
  }

  @Override
  protected void onPreExecute() {
      callBack.preExecute();
  }

  @Override
  protected HomeRecyclerModel doInBackground(HomeRecyclerModel... params) {
      HomeRecyclerModel homeRecyclerModel = params[0];

      for (int i=0; i<homeRecyclerModel.getHomedata().size(); i++){
          HomeClass homeClass = homeRecyclerModel.getHomedata().get(i);
          homeClass.getTitle().setRendered(Html.fromHtml(homeClass.getTitle().getRendered()).toString());
          homeClass.getExcerpt().setRendered(Html.fromHtml(homeClass.getExcerpt().getRendered()).toString());
      }
      return homeRecyclerModel;
  }

}
