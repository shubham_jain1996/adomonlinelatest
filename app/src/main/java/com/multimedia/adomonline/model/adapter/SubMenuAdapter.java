package com.multimedia.adomonline.model.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.modelClass.player.SideMenuList;
import com.multimedia.adomonline.model.modelClass.player.SubMenuList;

import java.util.ArrayList;
import java.util.List;


public class SubMenuAdapter extends RecyclerView.Adapter<SubMenuAdapter.ViewHolder> {
    private Context context;
    List<SubMenuList> navigationData = new ArrayList<>();



    public SubMenuAdapter(Context context, List<SubMenuList> navigationData ) {
        this.context=context;
        this.navigationData= navigationData;

    }


    @Override
    public SubMenuAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.navigationitem, viewGroup, false);
        return new SubMenuAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(SubMenuAdapter.ViewHolder viewHolder, int position1) {
            viewHolder.navigationLayout.setBackgroundColor(context.getResources().getColor(R.color.orange));
            viewHolder.itemName.setText(navigationData.get(position1).getTitle());

    }
    @Override
    public int getItemCount() {
        return navigationData.size();
    }

     static class ViewHolder extends RecyclerView.ViewHolder {
       TextView itemName;
       ImageView itemImage;
       RelativeLayout navigationLayout;


         ViewHolder(View view) {
            super(view);
             navigationLayout=view.findViewById(R.id.navigationLayout);
            itemName=view.findViewById(R.id.itemName);
           itemImage=view.findViewById(R.id.ItemImage);


        }
    }


}