package com.multimedia.adomonline.model.modelClass.youtubeFiles;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class YoutubeVedioList implements Parcelable {
    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("etag")
    @Expose
    private String etag;
    @SerializedName("nextPageToken")
    @Expose
    private String nextPageToken;
    @SerializedName("pageInfo")
    @Expose
    private PageInfo pageInfo;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;


    protected YoutubeVedioList(Parcel in) {
        kind = in.readString();
        etag = in.readString();
        nextPageToken = in.readString();
        items = in.createTypedArrayList(Item.CREATOR);
    }

    public static final Creator<YoutubeVedioList> CREATOR = new Creator<YoutubeVedioList>() {
        @Override
        public YoutubeVedioList createFromParcel(Parcel in) {
            return new YoutubeVedioList(in);
        }

        @Override
        public YoutubeVedioList[] newArray(int size) {
            return new YoutubeVedioList[size];
        }
    };

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(kind);
        dest.writeString(etag);
        dest.writeString(nextPageToken);
        dest.writeTypedList(items);
    }
}
