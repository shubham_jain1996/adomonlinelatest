package com.multimedia.adomonline.model.tv;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.multimedia.adomonline.model.modelClass.youtubeFiles.Snippet;

public class ChannelItem implements Parcelable {
    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("etag")
    @Expose
    private String etag;
    @SerializedName("id")
    @Expose
    private Id id;
    @SerializedName("snippet")
    @Expose
    private Snippet snippet;


    protected ChannelItem(Parcel in) {
        kind = in.readString();
        etag = in.readString();
        snippet = in.readParcelable(Snippet.class.getClassLoader());
    }

    public static final Creator<ChannelItem> CREATOR = new Creator<ChannelItem>() {
        @Override
        public ChannelItem createFromParcel(Parcel in) {
            return new ChannelItem(in);
        }

        @Override
        public ChannelItem[] newArray(int size) {
            return new ChannelItem[size];
        }
    };

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Snippet getSnippet() {
        return snippet;
    }

    public void setSnippet(Snippet snippet) {
        this.snippet = snippet;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(kind);
        dest.writeString(etag);
        dest.writeParcelable(snippet, flags);
    }
}
