package com.multimedia.adomonline.model.adapter;

import android.content.Context;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;



import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.modelClass.TabLayoutModel;
import com.multimedia.adomonline.model.modelClass.player.SideMenuList;
import com.multimedia.adomonline.model.utility.RecyclerTouchListener;
import com.multimedia.adomonline.view.mainActivity.MainActivity;
import com.multimedia.adomonline.view.mainActivity.PlayerActivity;

import java.util.ArrayList;
import java.util.List;


public class NavigationAdapter extends RecyclerView.Adapter<NavigationAdapter.ViewHolder> {
    private Context context;
    List<SideMenuList> navigationData = new ArrayList<>();



    public NavigationAdapter(Context context, List<SideMenuList> navigationData ) {
        this.context=context;
        this.navigationData= navigationData;

    }


    @Override
    public NavigationAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.navigationitem, viewGroup, false);
        return new NavigationAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(NavigationAdapter.ViewHolder viewHolder, int position1) {


        if (navigationData.get(position1).getisSelected()) {
            viewHolder.navigationLayout.setBackgroundColor(context.getResources().getColor(R.color.brown));
        }else{
            viewHolder.navigationLayout.setBackgroundColor(context.getResources().getColor(R.color.orange));
        }
        viewHolder.layoutManager=new LinearLayoutManager(context);
        RecyclerView.LayoutManager lm = new GridLayoutManager(context, 1);
        viewHolder.subMenuView.setLayoutManager(lm);
        if (navigationData.get(position1).getExpandable()){
            viewHolder.indicator.setVisibility(View.VISIBLE);
            viewHolder.indicator.setImageDrawable(context.getResources().getDrawable(R.drawable.up_arrow));
            SubMenuAdapter subMenuAdapter = new SubMenuAdapter(context,navigationData.get(position1).getSubmenu());
            viewHolder.subMenuView.setAdapter(subMenuAdapter);
            if (navigationData.get(position1).getExpandableClick()){
                viewHolder.subMenuView.setVisibility(View.VISIBLE);

            }else {
                viewHolder.indicator.setImageDrawable(context.getResources().getDrawable(R.drawable.down_arrow));
                viewHolder.subMenuView.setVisibility(View.GONE);
            }

        }else {
            viewHolder.subMenuView.setVisibility(View.GONE);
            viewHolder.indicator.setVisibility(View.INVISIBLE);
        }

        viewHolder.itemName.setText(navigationData.get(position1).getTitle());
        viewHolder.itemImage.setImageResource(navigationData.get(position1).getIcon());
        viewHolder.subMenuView.addOnItemTouchListener(new RecyclerTouchListener(context, viewHolder.subMenuView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view,  int position) {
                ((MainActivity)context).setUpNavigationClickForChild(position1,position);

            }
            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        viewHolder.itemName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context).setUpNavigationClick(position1);
            }
        });
        viewHolder.indicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context).setUpNavigationClick(position1);
            }
        });
    }
    @Override
    public int getItemCount() {
        return navigationData.size();
    }

     static class ViewHolder extends RecyclerView.ViewHolder {
       TextView itemName;
       ImageView itemImage,indicator;
       RelativeLayout navigationLayout;
       RecyclerView subMenuView;
         LinearLayoutManager layoutManager;
         ViewHolder(View view) {
            super(view);
             subMenuView=view.findViewById(R.id.recyclerView);
             navigationLayout=view.findViewById(R.id.navigationLayout);
            itemName=view.findViewById(R.id.itemName);
             indicator=view.findViewById(R.id.indicator);
           itemImage=view.findViewById(R.id.ItemImage);


        }
    }


}