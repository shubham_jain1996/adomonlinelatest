package com.multimedia.adomonline.model.modelClass.homePageModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContentModel implements Parcelable
{
    public ContentModel(String rendered) {
        this.rendered = rendered;
    }

    @SerializedName("rendered")
    @Expose
    private String rendered;
    public final static Parcelable.Creator<ContentModel> CREATOR = new Creator<ContentModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ContentModel createFromParcel(Parcel in) {
            return new ContentModel(in);
        }

        public ContentModel[] newArray(int size) {
            return (new ContentModel[size]);
        }

    };

    protected ContentModel(Parcel in) {
        this.rendered = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ContentModel() {
    }

    public String getRendered() {
        return rendered;
    }

    public void setRendered(String rendered) {
        this.rendered = rendered;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(rendered);
    }

    public int describeContents() {
        return 0;
    }
}
