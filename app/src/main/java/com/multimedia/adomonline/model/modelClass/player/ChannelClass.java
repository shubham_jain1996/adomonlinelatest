package com.multimedia.adomonline.model.modelClass.player;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChannelClass {
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("isplay")
    @Expose
    private Boolean isplay;
    public ChannelClass(String url, Boolean isplay) {
        this.url = url;
        this.isplay = isplay;
    }




    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getIsplay() {
        return isplay;
    }

    public void setIsplay(Boolean isplay) {
        this.isplay = isplay;
    }


}
