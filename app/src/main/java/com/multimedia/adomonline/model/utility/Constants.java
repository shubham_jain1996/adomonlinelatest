package com.multimedia.adomonline.model.utility;



public class Constants {


    public static final Boolean STATUS =true ;
    public static final String PREF_NAME ="subcon" ;

    public interface ACTION {
        public static String PLAY_ACTION = "play";
        public static String PAUSE_ACTION = "pause";
        public static String PRIVIOUS_ACTION = "backword";
        public static String FORWORD_ACTION = "forword";

    }

    public class Pref {
        public static final String PLAYERSTATE ="playerPosition" ;
    }
}
