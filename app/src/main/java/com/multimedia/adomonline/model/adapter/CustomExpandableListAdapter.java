package com.multimedia.adomonline.model.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.modelClass.player.SideMenuList;

import java.util.HashMap;
import java.util.List;

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<SideMenuList> expandableListTitle;


    public CustomExpandableListAdapter(Context context, List<SideMenuList> expandableListTitle
                                       ) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;

    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return expandableListTitle.get(listPosition).getSubmenu();

    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
      //   String expandedListText = expandableListTitle.get(listPosition).getSubmenu().get(expandedListPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.navigationitem, null);
        }
        TextView expandedListTextView
                = (TextView) convertView
                .findViewById(R.id.itemName);
        expandedListTextView.setTypeface(null, Typeface.BOLD);
   //     expandedListTextView.setText(expandedListText);
     /*   convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, expandedListText, Toast.LENGTH_SHORT).show();
            }
        });*/
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        if (expandableListTitle.get(listPosition).getSubmenu()!=null){
            return expandableListTitle.get(listPosition).getSubmenu()
                    .size();
        }else {
            return 0;
        }

    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String listTitle =expandableListTitle.get(listPosition).getTitle() ;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.navigationitem, null);
        }
        TextView listTitleTextView
         = (TextView) convertView
                .findViewById(R.id.itemName);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);
      /*  convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(context, listTitle, Toast.LENGTH_SHORT).show();
            }
        });*/
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}