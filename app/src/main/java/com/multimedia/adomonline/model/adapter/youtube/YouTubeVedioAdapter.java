package com.multimedia.adomonline.model.adapter.youtube;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.modelClass.youtubeFiles.Item;
import com.multimedia.adomonline.model.modelClass.youtubeFiles.YoutubeVedioList;
import com.multimedia.adomonline.model.util.Util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class YouTubeVedioAdapter extends RecyclerView.Adapter<YouTubeVedioAdapter.ViewHolder> {
    private Context context;
    YoutubeVedioList data;
    callMore callMore;
    List<Item> youtubeVedios= new ArrayList<>();


    public YouTubeVedioAdapter(Context context, List<Item> data, callMore callMore) {
        this.context = context;
        this.youtubeVedios=data;
        this.callMore=callMore;

    }


    @Override
    public YouTubeVedioAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.youtube_adapter, viewGroup, false);
        return new YouTubeVedioAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(YouTubeVedioAdapter.ViewHolder viewHolder, int position) {
        if(position==(getItemCount()-1)){
            callMore.callMoreData();
        }
        Picasso.get().load("https://i.ytimg.com/vi/"+youtubeVedios.get(position).getSnippet().getResourceId().getVideoId()+"/sddefault.jpg").error(R.drawable.new_adom_logo).into(viewHolder.vedioImage, new Callback() {
            @Override
            public void onSuccess() {
                viewHolder.progress_view.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                viewHolder.progress_view.setVisibility(View.GONE);
            }
        });
        viewHolder.title.setText(youtubeVedios.get(position).getSnippet().getTitle());
        viewHolder.vedioTime.setText(Util.parseDatefromServer(youtubeVedios.get(position).getSnippet().getPublishedAt()));

    }
    @Override
    public int getItemCount() {
        if (youtubeVedios!=null){
            return youtubeVedios.size();
        }else{
            return 0;
        }

    }

     static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView vedioImage;
        TextView title,vedioTime;
        CircularProgressView progress_view;
         ViewHolder(View view) {
            super(view);
             progress_view=view.findViewById(R.id.progress_view);
             vedioImage=view.findViewById(R.id.vedioImage);
             title=view.findViewById(R.id.title);
             vedioTime=view.findViewById(R.id.vedioTime);



        }
    }
    public interface callMore{
        void callMoreData();
    }
}