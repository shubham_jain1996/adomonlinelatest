package com.multimedia.adomonline.model.player;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.multimedia.adomonline.view.mainActivity.PlayerActivity;

public class PlayerNotificationClick extends BroadcastReceiver  {

    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()){
            case "play":
                context.sendBroadcast(new Intent("play"));
                break;
            case "rewind":
                context.sendBroadcast(new Intent("rewind"));
                break;
            case "forword":
                context.sendBroadcast(new Intent("forword"));
                break;

        }


    }
}
