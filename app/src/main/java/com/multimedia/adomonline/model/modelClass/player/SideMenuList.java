package com.multimedia.adomonline.model.modelClass.player;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SideMenuList {
    @SerializedName("Title")
    @Expose
    private String Title;



    @SerializedName("Icon")
    @Expose
    private Integer icon;

    @SerializedName("Id")
    @Expose
    private Integer id;

    @SerializedName("isPlay")
    @Expose
    private Boolean isSelected;

    private Boolean isExpandable;

    private Boolean isExpandableClick=false;

    public Boolean getExpandableClick() {
        return isExpandableClick;
    }

    public void setExpandableClick(Boolean expandableClick) {
        isExpandableClick = expandableClick;
    }

    public Boolean getExpandable() {
        return isExpandable;
    }

    public void setExpandable(Boolean expandable) {
        isExpandable = expandable;
    }

    public SideMenuList(String title, Integer icon, Integer id, Boolean isSelected, Boolean isExpandable, List<SubMenuList> submenu) {
        Title = title;
        this.icon = icon;
        this.id = id;
        this.isSelected = isSelected;
        this.isExpandable = isExpandable;
        this.submenu = submenu;
    }

    public List<SubMenuList> getSubmenu() {
        return submenu;
    }

    public void setSubmenu(List<SubMenuList> submenu) {
        this.submenu = submenu;
    }

    private List<SubMenuList> submenu=null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public SideMenuList() {
    }


    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Boolean getisSelected() {
        return isSelected;
    }

    public void setisSelected(Boolean isPlay) {
        this.isSelected = isPlay;
    }

    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }





}
