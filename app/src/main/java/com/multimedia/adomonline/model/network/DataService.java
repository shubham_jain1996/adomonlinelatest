package com.multimedia.adomonline.model.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.modelClass.GetNonce;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;
import com.multimedia.adomonline.model.modelClass.bannerData.Bannerdatamodel;
import com.multimedia.adomonline.model.modelClass.youtubeFiles.YoutubeVedioList;
import com.multimedia.adomonline.model.tv.YoutubeChannelList;
import com.multimedia.adomonline.model.utility.InternetConnection;
import com.multimedia.adomonline.view.dialoge.ProgressDialogue;


import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataService {
    private String message = "Loading";
    Context context;


    public interface ServerResponseListner<T> {
        public void onDataSuccess(T response, int code);
        public void onDataFailiure();
    }
    public DataService(Context context)
    {
        helpDialog= new ProgressDialogue(context);
        helpDialog.setCancelable(false);
        this.context = context;
    }

    public DataService(){

    }

    private ServerResponseListner serverResponseListner;
    public void setOnServerListener(ServerResponseListner listener) {
        serverResponseListner = listener;
    }

    ProgressDialogue helpDialog;
    private ProgressDialog progressDialogue;

    private void showLoader(String message){
            if (helpDialog!=null){
                if (helpDialog.isShowing()){
                    helpDialog.dismiss();
                }
            }
        helpDialog.show();
    }

    public void getNonce() {

       Call<GetNonce> call = RetrofitInstance.getApiInstance().getNonce("user","register");
        performServerRequest(call);

    }


    public void getHome() {

        Call<List<HomeClass>> call = RetrofitInstance.getApiInstance().getHome("title",
                "date","excerpt","categories","id","featured_media","link","5");
        performServerRequest(call);

    }


    public void getSearchResult(String searchItem) {
        Call<List<HomeClass>> call = RetrofitInstance.getApiInstance().getSearchResult(searchItem,"title",
                "date","excerpt","categories","id","featured_media","link");
        performServerRequest(call);

    }


    public Call getBannerData(String mediaId) {

        Call<Bannerdatamodel> call = RetrofitInstance.getApiInstance().getBannerData(mediaId,"guid","date","source_url","media_details");
        performServerRequest(call);
        return call;
    }

    public void getTopStoriesData(String mediaId,String pageNo) {

        Call<List<HomeClass>> call = RetrofitInstance.getApiInstance().getTopStoriesData(mediaId,"title",
                "date","excerpt","categories","id","featured_media","link","4",pageNo);
        performServerRequest(call);

    }

    public void getallData(String mediaId,String pageNo) {

        Call<List<HomeClass>> call = RetrofitInstance.getApiInstance().getallData(mediaId,"title",
                "date","excerpt","categories","id","featured_media","link","20",pageNo);
        performServerRequest(call);

    }

    public void getArticleDetails(String position) {
           if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
           showLoader(message);
        Call<HomeClass> call = RetrofitInstance.getApiInstance().getArticleDetails(position);
        performServerRequest(call);

    }
    public void getArticleDetailsUsingSlug(String slug) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(message);
        Call<List<HomeClass>> call = RetrofitInstance.getApiInstance().getArticleDetailsUsingSlug(slug);
        performServerRequest(call);

    }


    public void getDataUsingId(String id) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }

        Call<HomeClass> call = RetrofitInstance.getApiInstance().getDataUsingId(id,"content","date","featured_media");
        performServerRequest(call);

    }
    public void getYouTubeVedios4(String playListId) {
        /*if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }*/
        // showLoader(message);
        Call<YoutubeVedioList> call = RetrofitInstance.getApiInstance().getYouTubeVedios4(playListId,"AIzaSyAmldoK5FMhNKOV410GfHVwJjH2qjomISU","snippet",
                "4");
        performServerRequest(call);

    }

    public void getYouTubeVedios(String playListId,String token) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(message);
        Call<YoutubeVedioList> call = RetrofitInstance.getApiInstance().getYouTubeVedios(playListId,"AIzaSyAmldoK5FMhNKOV410GfHVwJjH2qjomISU","snippet",
                "20",token);
        performServerRequest(call);

    }

    public void getAdomTvChannelId() {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(message);
        Call<YoutubeChannelList> call = RetrofitInstance.getApiInstance().getAdomTvResult("UCKlgbbF9wphTKATOWiG5jPQ","AIzaSyAmldoK5FMhNKOV410GfHVwJjH2qjomISU","snippet",
                "live","video");
        performServerRequest(call);


    }


    public void getMyJoyChannelId() {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(message);
        Call<YoutubeChannelList> call = RetrofitInstance.getApiInstance().getAdomTvResult("UChd1DEecCRlxaa0-hvPACCw","AIzaSyAmldoK5FMhNKOV410GfHVwJjH2qjomISU","snippet",
                "live","video");
        performServerRequest(call);

    }


























    private void performServerRequest(Call call)
    {
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call call, Response response) {

                if (helpDialog != null) {
                  helpDialog.dismiss();
                }

                if (serverResponseListner != null ) {
                    serverResponseListner.onDataSuccess(response.body(),response.code());
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
             //   kProgressHUD.dismiss();
                if (helpDialog != null) {
                   helpDialog.dismiss();
                }

                if (serverResponseListner != null) {
                    serverResponseListner.onDataFailiure();
                }
                if (context != null) {
                    if (t instanceof IOException) {
                    }
                    else {
                        Toast.makeText(context, "Parsing Error", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });
    }



}

