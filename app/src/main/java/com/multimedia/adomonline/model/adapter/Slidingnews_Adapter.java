package com.multimedia.adomonline.model.adapter;

import android.content.Context;
import android.os.Parcelable;
import androidx.viewpager.widget.PagerAdapter;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;
import com.multimedia.adomonline.model.modelClass.bannerData.Bannerdatamodel;
import com.multimedia.adomonline.model.network.DataService;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

public class Slidingnews_Adapter extends PagerAdapter {


    private List<HomeClass> bannerTextData=new ArrayList<>();

    private LayoutInflater inflater;
    private Context context;



    public Slidingnews_Adapter(Context context, List<Bannerdatamodel> Bannerdatamodels, List<HomeClass> bannerData) {
        this.context = context;
        this.bannerTextData=bannerData;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return bannerTextData.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slidingnews_layout, view, false);
        ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.background);
        ProgressBar progressBar;
        progressBar=imageLayout.findViewById(R.id.prograssIndicator);

        HomeClass homeClass = bannerTextData.get(position);
        if (homeClass.getMedia_url()!=null){
            Picasso.get().load(homeClass.getMedia_url()).into(imageView, new Callback() {
                @Override
                public void onSuccess() {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }else{
            downLoadImages(imageView,homeClass,progressBar);
        }

        TextView textView1= imageLayout.findViewById(R.id.heading);
        textView1.setText(Html.fromHtml(homeClass.getTitle().getRendered()));
        view.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
    public void downLoadImages(ImageView imageView, HomeClass data,ProgressBar progressBar ){
        DataService dataService = new DataService();
        dataService.setOnServerListener(new DataService.ServerResponseListner<Bannerdatamodel>() {
            @Override
            public void onDataSuccess(Bannerdatamodel response, int code) {
                if (response != null && code==200) {
                    try {
                        data.setMedia_url(response.getSourceUrl());
                        Picasso.get().load(data.getMedia_url()).into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                progressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception e) {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                    }catch (Exception e){

                    }

                }

            }

            @Override
            public void onDataFailiure() {
            }
        });
        dataService.getBannerData(String.valueOf(data.getFeaturedMedia()));

    }
}
