package com.multimedia.adomonline.model.utility;

import com.multimedia.adomonline.model.modelClass.bannerData.HomeRecyclerModel;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;

import java.util.List;

public interface DataLoadingInterfaceHome {
    void preExecute();
    void postExecute(HomeRecyclerModel homeClasses);
}
