package com.multimedia.adomonline.model.adapter;




import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.multimedia.adomonline.model.modelClass.player.SideMenuList;
import com.multimedia.adomonline.view.mainActivity.fragment.Home;
import com.multimedia.adomonline.view.mainActivity.fragment.OtherTabFragment;

import java.util.ArrayList;
import java.util.List;


public  class ViewPagerAdapter extends FragmentPagerAdapter {
    public List<SideMenuList> categoryname = new ArrayList<>();

    private FragmentManager mFragmentManager;
    private Fragment mFragmentAtPos0;

    public ViewPagerAdapter(FragmentManager manager, List<SideMenuList> categoryname) {
        super(manager);
        this.categoryname= categoryname;
        mFragmentManager = manager;
    }
    @Override
    public int getItemPosition(Object object) {

            return POSITION_NONE;

    }
    @Override
    public Fragment getItem(int position) {
        if (position==0) {
            return new Home();
        }else{
            return new OtherTabFragment().newInstance(categoryname.get(position).getTitle(),categoryname.get(position).getId());
        }
    }

    @Override
    public int getCount() {
        return categoryname.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return categoryname.get(position).getTitle();
    }


}
