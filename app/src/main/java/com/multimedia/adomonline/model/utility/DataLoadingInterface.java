package com.multimedia.adomonline.model.utility;

import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;

import java.util.List;

public interface DataLoadingInterface {
    void preExecute();
    void postExecute(List<HomeClass> homeClasses);
}
