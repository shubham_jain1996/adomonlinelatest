package com.multimedia.adomonline.model.modelClass.bannerData;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;

import java.util.ArrayList;
import java.util.List;

public class HomeRecyclerModel implements Parcelable {
    @SerializedName("category_Name")
    @Expose
    private String category_Name;
    @SerializedName("category_Id")
    @Expose
    private String category_Id;

    @SerializedName("data")
    @Expose
    private List<HomeClass> topStories = new ArrayList<HomeClass>();

    public HomeRecyclerModel() {
    }
    public HomeRecyclerModel(String category_Name, String category_Id) {
        this.category_Name = category_Name;
        this.category_Id = category_Id;
        this.topStories = topStories;
    }
    public HomeRecyclerModel(String category_Name, String category_Id, List<HomeClass> topStories) {
        this.category_Name = category_Name;
        this.category_Id = category_Id;
        this.topStories = topStories;
    }

    protected HomeRecyclerModel(Parcel in) {
        category_Name = in.readString();
        category_Id = in.readString();
        topStories = in.createTypedArrayList(HomeClass.CREATOR);
    }

    public static final Creator<HomeRecyclerModel> CREATOR = new Creator<HomeRecyclerModel>() {
        @Override
        public HomeRecyclerModel createFromParcel(Parcel in) {
            return new HomeRecyclerModel(in);
        }

        @Override
        public HomeRecyclerModel[] newArray(int size) {
            return new HomeRecyclerModel[size];
        }
    };

    public String getCategory_Name() {
        return category_Name;
    }

    public void setCategory_Name(String category_Name) {
        this.category_Name = category_Name;
    }

    public String getCategory_Id() {
        return category_Id;
    }

    public void setCategory_Id(String category_Id) {
        this.category_Id = category_Id;
    }

    public List<HomeClass> getHomedata() {
        return topStories;
    }

    public void setHomeData(List<HomeClass> topStories) {
        this.topStories = topStories;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(category_Name);
        dest.writeString(category_Id);
        dest.writeTypedList(topStories);
    }
}
