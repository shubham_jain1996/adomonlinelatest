package com.multimedia.adomonline.model.utility;

import android.os.AsyncTask;
import android.text.Html;

import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;

import java.util.List;

public class ConvertHtmlToString extends AsyncTask<List<HomeClass>, Void, List<HomeClass>> {
  private DataLoadingInterface callBack;
  public ConvertHtmlToString(DataLoadingInterface dataLoading){
      this.callBack = dataLoading;
  }
  @Override
  protected void onPostExecute(List<HomeClass> homeClasses) {

      callBack.postExecute(homeClasses);
  }

  @Override
  protected void onPreExecute() {
      callBack.preExecute();
  }

  @Override
  protected List<HomeClass> doInBackground(List<HomeClass>... params) {

      List <HomeClass> homeData = params[0];
      for (int i=0; i<homeData.size(); i++){
          HomeClass homeClass = homeData.get(i);
          homeClass.getTitle().setRendered(Html.fromHtml(homeClass.getTitle().getRendered()).toString());
          homeClass.getExcerpt().setRendered(Html.fromHtml(homeClass.getExcerpt().getRendered()).toString());
      }
      return homeData;
  }

}
