package com.multimedia.adomonline.model.adapter;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.modelClass.bannerData.HomeRecyclerModel;
import com.multimedia.adomonline.view.mainActivity.Details;
import com.multimedia.adomonline.view.mainActivity.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class FragmentDataAdapter extends RecyclerView.Adapter {

        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_ITEM1 = 1;

    private SportsNewsAdapter sportsNewAdapter;
    private  StoriesNewsAdapter storiesnewsAdapter;
    Context context;
    List<HomeRecyclerModel> allData= new ArrayList<>();
    private Slidingnews_Adapter slidingAdapter;

    public FragmentDataAdapter(Context context, List<HomeRecyclerModel> allData) {
        this.context=context;
        this.allData=allData;
    }


        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            if (viewType == VIEW_TYPE_ITEM) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.singledata_fragment, parent, false);
                return new ItemViewHolder(view);
            }else  {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.singledata_fragment, parent, false);
                return new ItemViewHolder1(view);
            }
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
            if (viewHolder instanceof ItemViewHolder) {
                ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
                itemViewHolder.text.setText(allData.get(position).getCategory_Name());
                populateItemRows((ItemViewHolder) viewHolder, position);
                sportsNewAdapter = new SportsNewsAdapter(context, allData.get(position));
                sportsNewAdapter.setRowIndex(position);
                itemViewHolder.data.setAdapter(sportsNewAdapter);
                if (allData.get(position).getHomedata().size()>0){
                    itemViewHolder.itemView.setVisibility(View.VISIBLE);
                }else {
                    itemViewHolder.itemView.setVisibility(View.INVISIBLE);
                }
                itemViewHolder.moreStories.setOnClickListener(new ViewMoreAction(position));
            }
            else if (viewHolder instanceof ItemViewHolder1){
                ItemViewHolder1 itemViewHolder = (ItemViewHolder1) viewHolder;
                itemViewHolder.text.setText(allData.get(position).getCategory_Name());
                populateItemRows1((ItemViewHolder1) viewHolder, position);
                storiesnewsAdapter = new StoriesNewsAdapter(context, allData.get(position));
                itemViewHolder.data.setAdapter(storiesnewsAdapter);
                if (allData.get(position).getHomedata().size()>0){
                    itemViewHolder.itemView.setVisibility(View.VISIBLE);
                }else {
                    itemViewHolder.itemView.setVisibility(View.INVISIBLE);
                }
               itemViewHolder.moreStories.setOnClickListener(new ViewMoreAction(position));

            }
        }

    class ViewMoreAction implements  View.OnClickListener{
        int position = 0;
        ViewMoreAction(Integer position){
            this.position = position;
        }
        @Override
        public void onClick(View v) {
            MainActivity.adCounter+=1;
            Intent i = new Intent(context,Details.class);
            i.putExtra("categoryType",allData.get(position).getCategory_Name());
            i.putExtra("position",allData.get(position).getCategory_Id());
            context.startActivity(i);
        }
     }
        @Override
        public int getItemCount() {
            return allData.size();
        }


        @Override
        public int getItemViewType(int position) {
            if (position % 2 == 1) {
                return VIEW_TYPE_ITEM;
            } else {
                return VIEW_TYPE_ITEM1;
            }
        }


        private class ItemViewHolder extends RecyclerView.ViewHolder {
           RecyclerView data;
            TextView text;
            Button moreStories;
            public ItemViewHolder(@NonNull View itemView) {
                super(itemView);
                itemView.setTag(0);
                moreStories=itemView.findViewById(R.id.moreStories);
                text=itemView.findViewById(R.id.text);
                data=itemView.findViewById(R.id.data);
            }
        }
    private class ItemViewHolder1 extends RecyclerView.ViewHolder {
            Button moreStories;
        RecyclerView data;
        LinearLayoutManager layoutManager;
        TextView text;
        public ItemViewHolder1(@NonNull View itemView) {
            super(itemView);
            itemView.setTag(1);
            moreStories=itemView.findViewById(R.id.moreStories);
            text=itemView.findViewById(R.id.text);
            data=itemView.findViewById(R.id.data);
        }
    }




        private void populateItemRows(ItemViewHolder viewHolder, int i) {

            viewHolder.data.setHasFixedSize(true);
            RecyclerView.LayoutManager verticalLayoutManager = new GridLayoutManager(context, 2);
            viewHolder.data.setLayoutManager(verticalLayoutManager);
        }

    private void populateItemRows1(ItemViewHolder1 viewHolder, int position) {
        viewHolder.data.setHasFixedSize(true);
        viewHolder.layoutManager=new LinearLayoutManager(context);
        viewHolder.data.setLayoutManager(viewHolder.layoutManager);
    }


}

