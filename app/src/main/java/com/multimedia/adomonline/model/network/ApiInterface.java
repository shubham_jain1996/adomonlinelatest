package com.multimedia.adomonline.model.network;


import com.multimedia.adomonline.model.modelClass.GetNonce;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;
import com.multimedia.adomonline.model.modelClass.bannerData.Bannerdatamodel;
import com.multimedia.adomonline.model.modelClass.youtubeFiles.YoutubeVedioList;
import com.multimedia.adomonline.model.tv.YoutubeChannelList;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

   /* @GET("/get_nonce/?controller=user&method=register")
    Call<GetNonce> getNonce();*/

    /*@GET("/wp-json/wp/v2/categories")
    Call<List<HomeModel>> getHomeData();*/


    @FormUrlEncoded
    @POST("api/get_nonce/")
    Call<GetNonce> getNonce(@Field("controller") String action, @Field("method") String name);



    @GET("wp-json/wp/v2/posts")
    Call<List<HomeClass>> getSearchResult(@Query("search") String search,@Query("_fields[]") String title1,@Query("_fields[]") String title,@Query("_fields[]") String data
            ,@Query("_fields[]") String excert,@Query("_fields[]") String catregory,
                                      @Query("_fields[]") String content,@Query("_fields[]") String Id);
    @GET("wp-json/wp/v2/posts")
    Call<List<HomeClass>> getHome(@Query("_fields[]") String title,@Query("_fields[]") String data
            ,@Query("_fields[]") String excert,@Query("_fields[]") String catregory,
                                      @Query("_fields[]") String content,@Query("_fields[]") String Id,@Query("_fields[]") String link,@Query("per_page") String length);




    @GET("wp-json/wp/v2/media/{mediaId}")
    Call<Bannerdatamodel> getBannerData(@Path("mediaId") String medaId, @Query("_fields[]") String guid, @Query("_fields[]") String date, @Query("_fields[]") String sizes, @Query("_fields[]") String source_url);


    @GET("wp-json/wp/v2/posts")
    Call<List<HomeClass>> getTopStoriesData(@Query("categories") String medaId,@Query("_fields[]") String title,@Query("_fields[]") String data
    ,@Query("_fields[]") String excert,@Query("_fields[]") String catregory,
       @Query("_fields[]") String content,@Query("_fields[]") String Id,@Query("_fields[]") String link,@Query("per_page") String length,@Query("page") String page);

    @GET("wp-json/wp/v2/posts")
    Call<List<HomeClass>> getallData(@Query("categories") String medaId,@Query("_fields[]") String title,@Query("_fields[]") String data
            ,@Query("_fields[]") String excert,@Query("_fields[]") String catregory,
                                             @Query("_fields[]") String content,@Query("_fields[]") String Id,@Query("_fields[]") String link,@Query("per_page") String length,@Query("page") String page);


    @GET("wp-json/wp/v2/posts/{mediaId}")
    Call<HomeClass>getArticleDetails(@Path("mediaId") String medaId);

    @GET("wp-json/wp/v2/posts")
    Call<List<HomeClass>>getArticleDetailsUsingSlug(@Query("slug") String slug);

    @GET("wp-json/wp/v2/posts/{mediaId}")
    Call<HomeClass>getDataUsingId(@Path("mediaId") String medaId,@Query("_fields[]") String guid,@Query("_fields[]") String date,@Query("_fields[]") String media_id);

    @GET("https://www.googleapis.com/youtube/v3/playlistItems")
    Call<YoutubeVedioList> getYouTubeVedios4(@Query("playlistId") String id, @Query("key") String key
            , @Query("part") String part, @Query("maxResults") String maxResults);

    @GET("https://www.googleapis.com/youtube/v3/playlistItems")
    Call<YoutubeVedioList> getYouTubeVedios(@Query("playlistId") String id, @Query("key") String key
            , @Query("part") String part, @Query("maxResults") String maxResults, @Query("pageToken") String pageToken);

    @GET("https://www.googleapis.com/youtube/v3/search")
    Call<YoutubeChannelList> getAdomTvResult(@Query("channelId") String id, @Query("key") String key
            , @Query("part") String part, @Query("eventType") String maxResults, @Query("type") String pageToken);


}
