package com.multimedia.adomonline.model.modelClass.youtubeFiles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlayListModel {
    @SerializedName("Title")
    @Expose
    private String Title;


    @SerializedName("PlayListId")
    @Expose
    private String playListId;

    @SerializedName("Image_url")
    @Expose
    private String imageUrl;

    @SerializedName("data")
    @Expose
    private YoutubeVedioList topStories = null;

    public YoutubeVedioList getTopStories() {
        return topStories;
    }

    public void setTopStories(YoutubeVedioList topStories) {
        this.topStories = topStories;
    }

    public PlayListModel(String title, String playListId, String imageUrl, YoutubeVedioList topStories) {
        Title = title;
        this.playListId = playListId;
        this.imageUrl = imageUrl;
        this.topStories = topStories;
    }

    public PlayListModel(YoutubeVedioList topStories) {
        this.topStories = topStories;
    }

    public PlayListModel() {
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getPlayListId() {
        return playListId;
    }

    public void setPlayListId(String playListId) {
        this.playListId = playListId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
