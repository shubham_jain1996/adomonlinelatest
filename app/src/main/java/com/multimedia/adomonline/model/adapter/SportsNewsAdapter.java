package com.multimedia.adomonline.model.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Parcelable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;



import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;
import com.multimedia.adomonline.model.modelClass.bannerData.Bannerdatamodel;
import com.multimedia.adomonline.model.modelClass.bannerData.HomeRecyclerModel;
import com.multimedia.adomonline.model.network.DataService;
import com.multimedia.adomonline.model.util.Util;
import com.multimedia.adomonline.view.mainActivity.Article;
import com.multimedia.adomonline.view.mainActivity.MainActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class SportsNewsAdapter extends RecyclerView.Adapter<SportsNewsAdapter.ViewHolder> {
    Context mContext;
    private int mRowIndex = -1;
    HomeRecyclerModel topStoriesList;



    public SportsNewsAdapter(Context context, HomeRecyclerModel topStoriesList) {
        this.mContext = context;
        this.topStoriesList = topStoriesList;
    }

    public void setRowIndex(int index) {
        mRowIndex = index;
    }



    @NonNull
    @Override
    public SportsNewsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.rvsports1item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SportsNewsAdapter.ViewHolder viewHolder, int position) {
        HomeClass homeClass= topStoriesList.getHomedata().get(position);
        viewHolder.newsTime.setText(Util.parseDatefromServer(homeClass.getDate()));
              viewHolder.text.setText(homeClass.getTitle().getRendered());
            viewHolder.itemView.setTag(mRowIndex);
        viewHolder.brandPic.setImageResource(R.drawable.new_adom_logo);
        if (homeClass.getMedia_url()!=null){
            Picasso.get().load(homeClass.getMedia_url()).placeholder(R.drawable.new_adom_logo).into(viewHolder.brandPic);
        }else{
            downLoadImages(viewHolder.brandPic,homeClass);
        }
       viewHolder.newsType.setText(topStoriesList.getCategory_Name());
        viewHolder.newsTime.setText(Util.parseDatefromServer(homeClass.getDate()));
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.adCounter+=1;
                Intent i =new Intent(mContext, Article.class);
                i.putExtra("categoryType",topStoriesList.getCategory_Name());
                i.putExtra("model", topStoriesList.getHomedata().get(position));
                i.putParcelableArrayListExtra("data", (ArrayList<? extends Parcelable>) topStoriesList.getHomedata());
                i.putExtra("position",  position);
                mContext.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
                 return topStoriesList.getHomedata().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView brandPic;
        TextView text,newsTime,newsType;
        ProgressBar progressBar;

        public ViewHolder(View itemView) {
            super(itemView);
            brandPic = itemView.findViewById(R.id.image1);
            newsTime=itemView.findViewById(R.id.newsTime);
            text=itemView.findViewById(R.id.text1);
            newsType=itemView.findViewById(R.id.newsType);

        }
    }
    public void downLoadImages(ImageView imageView, HomeClass data ){
        DataService dataService = new DataService();
        dataService.setOnServerListener(new DataService.ServerResponseListner<Bannerdatamodel>() {
            @Override
            public void onDataSuccess(Bannerdatamodel response, int code) {
                if (response != null && code==200) {
                    try {
                        if (response.getMediaDetails().getSizes().getMedium() != null) {
                            data.setMedia_url(response.getMediaDetails().getSizes().getMedium().getSourceUrl());
                        } else if (response.getMediaDetails().getSizes().getThumbnail() != null) {
                            data.setMedia_url(response.getMediaDetails().getSizes().getThumbnail().getSourceUrl());
                        } else {
                            data.setMedia_url(response.getSourceUrl());
                        }
                        data.setSource_url(response.getSourceUrl());
                        Picasso.get().load(data.getMedia_url()).placeholder(R.drawable.new_adom_logo).into(imageView);
                    }catch (Exception e){
                        imageView.setImageResource(R.drawable.new_adom_logo);
                    }

                }else{
                    imageView.setImageResource(R.drawable.new_adom_logo);
                }

            }

            @Override
            public void onDataFailiure() {
            }
        });
        dataService.getBannerData(String.valueOf(data.getFeaturedMedia()));

    }


}
