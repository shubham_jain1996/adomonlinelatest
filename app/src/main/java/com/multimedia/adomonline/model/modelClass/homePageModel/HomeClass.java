package com.multimedia.adomonline.model.modelClass.homePageModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeClass implements Parcelable
{
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("guid")
    @Expose
    private ContentModel guid;

    protected HomeClass(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        date = in.readString();
        guid = in.readParcelable(ContentModel.class.getClassLoader());
        link = in.readString();
        title = in.readParcelable(ContentModel.class.getClassLoader());
        content = in.readParcelable(ContentModel.class.getClassLoader());
        excerpt = in.readParcelable(ContentModel.class.getClassLoader());
        media_url = in.readString();
        source_url = in.readString();
        if (in.readByte() == 0) {
            featuredMedia = null;
        } else {
            featuredMedia = in.readInt();
        }
    }

    public static final Creator<HomeClass> CREATOR = new Creator<HomeClass>() {
        @Override
        public HomeClass createFromParcel(Parcel in) {
            return new HomeClass(in);
        }

        @Override
        public HomeClass[] newArray(int size) {
            return new HomeClass[size];
        }
    };

    public String getSlug() {
        return link;
    }

    public void setSlug(String slug) {
        this.link = slug;
    }

    @SerializedName("link")
    @Expose
    private String link;


    @SerializedName("title")
    @Expose
    private ContentModel title;
    @SerializedName("content")
    @Expose
    private ContentModel content;
    @SerializedName("excerpt")
    @Expose
    private ContentModel excerpt;

    String media_url;
    String source_url;



    public String getSource_url() {
        return source_url;
    }

    public void setSource_url(String source_url) {
        this.source_url = source_url;
    }



    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }




    public Integer getFeaturedMedia() {
        return featuredMedia;
    }

    public void setFeaturedMedia(Integer featuredMedia) {
        this.featuredMedia = featuredMedia;
    }

    @SerializedName("featured_media")
    @Expose
    private Integer featuredMedia;


    public HomeClass() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }



    public ContentModel getGuid() {
        return guid;
    }

    public void setGuid(ContentModel guid) {
        this.guid = guid;
    }

    public ContentModel getTitle() {
        return title;
    }

    public void setTitle(ContentModel title) {
        this.title = title;
    }

    public ContentModel getContent() {
        return content;
    }

    public void setContent(ContentModel content) {
        this.content = content;
    }

    public ContentModel getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(ContentModel excerpt) {
        this.excerpt = excerpt;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(date);
        dest.writeParcelable(guid, flags);
        dest.writeString(link);
        dest.writeParcelable(title, flags);
        dest.writeParcelable(content, flags);
        dest.writeParcelable(excerpt, flags);
        dest.writeString(media_url);
        dest.writeString(source_url);
        if (featuredMedia == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(featuredMedia);
        }
    }
}






