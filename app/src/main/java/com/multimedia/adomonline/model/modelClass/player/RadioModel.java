package com.multimedia.adomonline.model.modelClass.player;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RadioModel {
    @SerializedName("Title")
    @Expose
    private String Title;


    @SerializedName("Tagline")
    @Expose
    private String tagline;

    @SerializedName("source_url")
    @Expose
    private String sourceUrl;

    @SerializedName("Icon")
    @Expose
    private Integer icon;

    @SerializedName("isPlay")
    @Expose
    private Boolean isPlay;

    public RadioModel() {
    }

    public RadioModel(String title, String tagline, String sourceUrl, Integer icon, Boolean isPlay) {
        Title = title;
        this.tagline = tagline;
        this.sourceUrl = sourceUrl;
        this.icon = icon;
        this.isPlay = isPlay;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Boolean getIsPlay() {
        return isPlay;
    }

    public void setIsPlay(Boolean isPlay) {
        this.isPlay = isPlay;
    }

    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }

    public String getTagLine() {
        return tagline;
    }

    public void setTagLine(String tagline) {
        this.tagline = tagline;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }



}
