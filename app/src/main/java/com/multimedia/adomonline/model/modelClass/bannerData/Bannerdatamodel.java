package com.multimedia.adomonline.model.modelClass.bannerData;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class Bannerdatamodel implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;


    @SerializedName("media_details")
    @Expose
    private MediaDetails mediaDetails;
    @SerializedName("source_url")
    @Expose
    private String sourceUrl;




    protected Bannerdatamodel(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        mediaDetails = in.readParcelable(MediaDetails.class.getClassLoader());
        sourceUrl = in.readString();

    }

    public static final Creator<Bannerdatamodel> CREATOR = new Creator<Bannerdatamodel>() {
        @Override
        public Bannerdatamodel createFromParcel(Parcel in) {
            return new Bannerdatamodel(in);
        }

        @Override
        public Bannerdatamodel[] newArray(int size) {
            return new Bannerdatamodel[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MediaDetails getMediaDetails() {
        return mediaDetails;
    }

    public void setMediaDetails(MediaDetails mediaDetails) {
        this.mediaDetails = mediaDetails;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeParcelable(mediaDetails, flags);
        dest.writeString(sourceUrl);

    }
}
