package com.multimedia.adomonline.model.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.multimedia.adomonline.R;
import com.multimedia.adomonline.model.modelClass.homePageModel.HomeClass;
import com.multimedia.adomonline.model.modelClass.bannerData.Bannerdatamodel;
import com.squareup.picasso.Picasso;


import java.util.List;


public class InitialNewsAdapter extends RecyclerView.Adapter<InitialNewsAdapter.ViewHolder> {
    Context mContext;
    private int mRowIndex = -1;
   List<HomeClass> topStories;
   List<Bannerdatamodel> topStoriesImages;


    public InitialNewsAdapter(Context context, List<HomeClass> topStories, List<Bannerdatamodel> topStoriesImages) {
        this.mContext = context;
        this.topStories = topStories;
        this.topStoriesImages = topStoriesImages;
    }

    public void setRowIndex(int index) {
        mRowIndex = index;
    }



    @NonNull
    @Override
    public InitialNewsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.rvitem, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull InitialNewsAdapter.ViewHolder viewHolder, int position) {

                 viewHolder.text.setText(Html.fromHtml(topStories.get(position).getTitle().getRendered()));
                     viewHolder.itemView.setTag(mRowIndex);
        Picasso.get().load(topStoriesImages.get(position).getSourceUrl()).into( viewHolder.brandPic);



    }

    @Override
    public int getItemCount() {
             try {
                 return topStoriesImages.size();
             }catch (Exception e){
                 return 0;
             }


    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView brandPic,lock;
        TextView text;


        public ViewHolder(View itemView) {
            super(itemView);
            brandPic = itemView.findViewById(R.id.image1);
            text=itemView.findViewById(R.id.text1);

        }
    }
}
