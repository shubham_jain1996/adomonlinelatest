package com.multimedia.adomonline.model.modelClass.bannerData;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sizes implements Parcelable
{

    @SerializedName("thumbnail")
    @Expose
    private Thumbnail thumbnail;

    @SerializedName("medium")
    @Expose
    private Thumbnail medium;

    @SerializedName("td_534x462")
    @Expose
    private Thumbnail td_534x462;

    protected Sizes(Parcel in) {
        thumbnail = in.readParcelable(Thumbnail.class.getClassLoader());
        medium = in.readParcelable(Thumbnail.class.getClassLoader());
        td_534x462 = in.readParcelable(Thumbnail.class.getClassLoader());
    }

    public static final Creator<Sizes> CREATOR = new Creator<Sizes>() {
        @Override
        public Sizes createFromParcel(Parcel in) {
            return new Sizes(in);
        }

        @Override
        public Sizes[] newArray(int size) {
            return new Sizes[size];
        }
    };

    public Thumbnail getThumbnail() {
        return thumbnail;
    }
    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Thumbnail getMedium() {
        return medium;
    }

    public void setMedium(Thumbnail medium) {
        this.medium = medium;
    }

    public Thumbnail getTd_534x462() {
        return td_534x462;
    }

    public void setTd_534x462(Thumbnail td_534x462) {
        this.td_534x462 = td_534x462;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(thumbnail, flags);
        dest.writeParcelable(medium, flags);
        dest.writeParcelable(td_534x462, flags);
    }
}



